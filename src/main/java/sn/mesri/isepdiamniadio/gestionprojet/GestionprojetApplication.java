package sn.mesri.isepdiamniadio.gestionprojet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.context.annotation.Import;
import org.springframework.web.client.RestTemplate;
import sn.mesri.isepdiamniadio.gestionprojet.config.CustomUserDetails;
import sn.mesri.isepdiamniadio.gestionprojet.dao.UtilisateurRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Utilisateur;
import sn.mesri.isepdiamniadio.gestionprojet.service.UtilisateurService;

import java.util.Arrays;
//import sn.mesri.isepdiamniadio.gestionprojet.config.DataSourceTestConfig;

@SpringBootApplication
//@EnableAutoConfiguration
@ComponentScan(basePackages = {
		"sn.mesri.isepdiamniadio.gestionprojet"
})
@PropertySource(value = {
		"application.properties",
		"application-dev.properties",
		"application-test.properties"
})
public class GestionprojetApplication {

	@Autowired
	private PasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(GestionprojetApplication.class, args);
	}

	@Autowired
	public void authenticationManager(AuthenticationManagerBuilder builder, UtilisateurRepository utilisateurRepository, UtilisateurService utilisateurService) throws Exception {
		builder.userDetailsService(userDetailsService(utilisateurRepository)).passwordEncoder(passwordEncoder);
	}

	private UserDetailsService userDetailsService(final UtilisateurRepository utilisateurRepository) {
		return username -> new CustomUserDetails(utilisateurRepository.findByUsername(username));
	}
}
