package sn.mesri.isepdiamniadio.gestionprojet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Projet;

import java.util.List;

public interface ProjetRepository extends JpaRepository<Projet,Long> {
    //@Query("SELECT p FROM projet WHERE p.nom = :nom")
    List<Projet> findByNom(@Param("nom") String nom);
}