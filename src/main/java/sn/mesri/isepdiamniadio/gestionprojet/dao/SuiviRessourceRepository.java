package sn.mesri.isepdiamniadio.gestionprojet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.SuiviRessource;

public interface SuiviRessourceRepository extends JpaRepository<SuiviRessource, Long> {
}
