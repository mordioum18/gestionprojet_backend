package sn.mesri.isepdiamniadio.gestionprojet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.ActiviteRessource;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.ActiviteRessourcePk;

public interface ActiviteRessourceRepository extends JpaRepository<ActiviteRessource,ActiviteRessourcePk> {
}
