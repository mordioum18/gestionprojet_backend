package sn.mesri.isepdiamniadio.gestionprojet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Decaissement;

public interface DecaissementRepository extends JpaRepository<Decaissement,Long> {
}
