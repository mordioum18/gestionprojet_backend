package sn.mesri.isepdiamniadio.gestionprojet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Activite;

public interface ActiviteRepository extends JpaRepository<Activite,Long> {
    //comment
}
