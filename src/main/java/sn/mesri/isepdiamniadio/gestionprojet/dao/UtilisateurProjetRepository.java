package sn.mesri.isepdiamniadio.gestionprojet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.UtilisateurProjet;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.UtilisateurProjetPk;

public interface UtilisateurProjetRepository extends JpaRepository<UtilisateurProjet, UtilisateurProjetPk> {
}
