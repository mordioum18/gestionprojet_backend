package sn.mesri.isepdiamniadio.gestionprojet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.LienTache;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.LienTachePk;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.UtilisateurProjet;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.UtilisateurProjetPk;

public interface LienTacheRepository extends JpaRepository<LienTache, LienTachePk> {
}
