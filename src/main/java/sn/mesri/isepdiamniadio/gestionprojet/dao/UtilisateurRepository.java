package sn.mesri.isepdiamniadio.gestionprojet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Utilisateur;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {
    Utilisateur findByUsername(String username);
    Utilisateur findByEmail(String username);
    Utilisateur findByTelephone(String username);
}
