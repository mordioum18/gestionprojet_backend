package sn.mesri.isepdiamniadio.gestionprojet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Ressource;

public interface RessourceRepository extends JpaRepository<Ressource,Long> {
}
