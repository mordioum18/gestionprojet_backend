package sn.mesri.isepdiamniadio.gestionprojet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Suivi;

public interface SuiviRepository extends JpaRepository<Suivi, Long>{
}
