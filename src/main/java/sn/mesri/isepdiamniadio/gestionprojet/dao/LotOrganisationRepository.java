package sn.mesri.isepdiamniadio.gestionprojet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.LotOrganisation;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.LotOrganisationPk;

public interface LotOrganisationRepository extends JpaRepository<LotOrganisation, LotOrganisationPk> {
}
