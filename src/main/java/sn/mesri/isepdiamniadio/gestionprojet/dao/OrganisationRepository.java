package sn.mesri.isepdiamniadio.gestionprojet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Organisation;

public interface OrganisationRepository extends JpaRepository<Organisation, Long> {
}
