package sn.mesri.isepdiamniadio.gestionprojet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Contrainte;

public interface ContrainteRepository extends JpaRepository<Contrainte,Long> {
}
