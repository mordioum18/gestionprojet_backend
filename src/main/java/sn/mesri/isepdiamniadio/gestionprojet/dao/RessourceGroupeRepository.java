package sn.mesri.isepdiamniadio.gestionprojet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.RessourceGroupe;

public interface RessourceGroupeRepository extends JpaRepository<RessourceGroupe,Long> {
}
