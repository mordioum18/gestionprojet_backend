package sn.mesri.isepdiamniadio.gestionprojet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.SuiviActivite;

public interface SuiviActiviteRepository extends JpaRepository<SuiviActivite, Long> {
}
