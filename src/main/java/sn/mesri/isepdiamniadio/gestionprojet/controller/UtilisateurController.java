package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Projet;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Utilisateur;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.UtilisateurProjet;
import sn.mesri.isepdiamniadio.gestionprojet.service.UtilisateurService;

import javax.validation.Valid;
import java.lang.reflect.Field;
import java.util.Collection;

@Api("Utilisateur")
@RestController
public class UtilisateurController {
    @Autowired
    private UtilisateurService utilisateurService;
    @Autowired
    private ConsumerTokenServices consumerTokenServices;

    @RequestMapping(value = "/api/utilisateurs",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Utilisateur> findAll(
            @SortDefault(sort = "nom", direction = Sort.Direction.ASC)
            @PageableDefault(value = 5) Pageable pageable){
        return utilisateurService.findAll(pageable);
    }
    @RequestMapping(value = "/api/utilisateurs/{id}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    //@PreAuthorize("hasPermission(#id, 'Utilisateur', 'ROLE_PROJET_CHEF_DE_PROJET')")
    public Utilisateur findById(@PathVariable("id") Long id){
        //Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return utilisateurService.findById(id);
    }

    @RequestMapping(value = "/api/utilisateurs-token",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Utilisateur findByToken(){
        return utilisateurService.findByToken();
    }

    @RequestMapping(value = "/utilisateurs-token",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void sendNewLien(@RequestParam(value = "email", required = true) String email){
        utilisateurService.sendLien(email);
    }

    @RequestMapping(value = "/api/utilisateurs/{id}",method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable ("id") Long id){
        utilisateurService.delete(id);
    }

    @RequestMapping(value = "/api/utilisateurs",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Utilisateur insert(@RequestBody @Valid Utilisateur utilisateur){

        return utilisateurService.insert(utilisateur);
    }
    @RequestMapping(value = "/api/utilisateurs/{id}",method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Utilisateur update(@PathVariable ("id") Long id, @RequestBody @Valid Utilisateur utilisateur){

        utilisateur.setId(id);
        utilisateur.setActif(true);
        return utilisateurService.update(utilisateur);
    }
    @RequestMapping(value = "/api/utilisateurs/{idUtilisateur}/projets",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<UtilisateurProjet> listProjetParticipe(@PathVariable Long idUtilisateur, Pageable pageable){
        return utilisateurService.listProjetParticipe(idUtilisateur,pageable);
    }
    @RequestMapping(value = "/api/utilisateurs/projets",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Projet> listProjetUserOnLine(){
        return utilisateurService.listProjetUserOnLine();
    }
    @RequestMapping(value = "/api/utilisateurs/recherche",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Utilisateur findByNameField(@RequestParam(value = "champ") String champ,
                                   @RequestParam(value = "valeur") String valeur){
        switch (champ.toLowerCase()){
            case "username":return utilisateurService.findByUsername(valeur);
            case "email":return utilisateurService.findByEmail(valeur);
            case "telephone":return utilisateurService.findByTelephone(valeur);
        }
        return null;
    }

    @RequestMapping(value = "/api/logout",method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void logout() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        OAuth2AuthenticationDetails detail = (OAuth2AuthenticationDetails) auth.getDetails();
        consumerTokenServices.revokeToken(detail.getTokenValue());
    }

}
