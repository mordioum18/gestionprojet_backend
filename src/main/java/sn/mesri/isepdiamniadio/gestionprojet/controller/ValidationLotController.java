package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Document;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.ValidationLot;
import sn.mesri.isepdiamniadio.gestionprojet.service.ValidationLotService;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.Collection;
import java.util.List;

@Api("ValidationLot")
@RestController
public class ValidationLotController {
    @Autowired
    private ValidationLotService validationLotService;


    @RequestMapping(value = "/validation-lots",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<ValidationLot> findAll(
            @SortDefault(sort = "dateValidation", direction = Sort.Direction.ASC)
            @PageableDefault(value = 5) Pageable pageable){
        return validationLotService.findAll(pageable);
    }

    @RequestMapping(value = "/api/validation-lots/{idValidationLot}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission(#id, 'ValidationLot', 'ADMIN')")
    public ValidationLot findById(@PathVariable Long idValidationLot,
                                  @RequestParam(name = "projet") Long idProjet){
        return validationLotService.findById(idProjet,idValidationLot);
    }

    @RequestMapping(value = "/validation-lots",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    //@ResponseStatus(HttpStatus.CREATED)
    public ValidationLot insert(@Valid @RequestBody ValidationLot validationLot,
                                @RequestParam(name = "projet") Long idProjet,
                                @RequestParam(name = "lot") Long idLot,
                                @RequestParam(name = "reunion", required = false) Long idReunion,
                                @RequestParam(name = "visite", required = false) Long idVisite) {
        return validationLotService.insert(validationLot,idProjet, idLot, idReunion, idVisite);
    }

    @RequestMapping(value = "/validation-lots/{idValidationLot}",method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ValidationLot update(@PathVariable Long idValidationLot,
                                @RequestParam(name = "projet") Long idProjet,
                                @RequestBody @Valid ValidationLot validationLot){
        validationLot.setId(idValidationLot);
        return validationLotService.update(validationLot,idProjet);
    }

    @RequestMapping(value = "/validation-lots/{idValidationLot}",method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long idValidationLot,
                       @RequestParam(name = "projet") Long idProjet){
        validationLotService.delete(idProjet,idValidationLot);
    }


    @ApiOperation(value = "avoir les documents concernant une validation d'un lot", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/validation-lots/{idValidationLot}/documents", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Document> getDocuments(@PathVariable Long idValidationLot,
                                             @RequestParam(name = "projet") Long idProjet) {
        return validationLotService.getDocuments(idProjet,idValidationLot);
    }
}
