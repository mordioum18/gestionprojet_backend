package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Financement;
import sn.mesri.isepdiamniadio.gestionprojet.service.FinancementService;

import javax.validation.Valid;
import java.util.Collection;

@Api("Financement")
@RestController
@RequestMapping(value = "/api")
public class FinancementController {
    @Autowired
    private FinancementService financementService;

    @RequestMapping(value = "/financements", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Financement> findAll(
            @SortDefault(sort = "dateFinancement", direction = Sort.Direction.ASC)
            @PageableDefault(value = 5) Pageable pageable){
        return financementService.findAll(pageable);
    }

    @RequestMapping(value = "/financements", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Financement insert(@Valid @RequestBody Financement financement,
                              @RequestParam(name = "projet") Long idProjet) {
        return financementService.insert(financement, idProjet);
    }

    @RequestMapping(value = "/financements/{idFinancement}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Financement findById(@PathVariable Long idFinancement,
                                @RequestParam(name = "projet") Long idProjet) {
        return financementService.findById(idProjet,idFinancement);
    }

    @RequestMapping(value = "/financements/{idFinancement}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Financement update(@RequestBody @Valid  Financement financement, @PathVariable Long idFinancement,
                              @RequestParam(name = "projet") Long idProjet) {
        financement.setId(idFinancement);
        return financementService.update(financement,idProjet);
    }

    @RequestMapping(value = "/financements/{idFinancement}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long idFinancement,
                       @RequestParam(name = "projet") Long idProjet) {
        financementService.delete(idProjet,idFinancement);
    }
}
