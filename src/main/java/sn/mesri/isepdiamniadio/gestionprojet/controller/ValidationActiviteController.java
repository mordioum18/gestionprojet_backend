package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Document;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.ValidationActivite;
import sn.mesri.isepdiamniadio.gestionprojet.service.ValidationActiviteService;

import javax.validation.Valid;
import java.util.Collection;

@Api("ValidationActivité")
@RestController
public class ValidationActiviteController {
    @Autowired
    private ValidationActiviteService validationActiviteService;

    @RequestMapping(value = "/validation-activites", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<ValidationActivite> findAll(
            @SortDefault(sort = "dateValidation", direction = Sort.Direction.ASC)
            @PageableDefault(value = 5) Pageable pageable){
        return validationActiviteService.findAll(pageable);
    }

    @RequestMapping(value = "/validation-activites/{idValidationActivite}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ValidationActivite findById(@PathVariable Long idValidationActivite,
                                       @RequestParam(name = "projet") Long idProjet) {
        return validationActiviteService.findById(idProjet,idValidationActivite);
    }

    @RequestMapping(value = "/validation-activites", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ValidationActivite insert(ValidationActivite validationActivite,
                                     @RequestParam(name = "projet") Long idProjet,
                                     @RequestParam(name = "activite") Long idActivite,
                                     @RequestParam(name = "reunion", required = false) Long idReunion,
                                     @RequestParam(name = "visite", required = false) Long idVisite) {
        return validationActiviteService.insert(validationActivite, idProjet, idActivite, idReunion, idVisite);
    }

    @RequestMapping(value = "/validation-activites/{idValidationActivite}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ValidationActivite update(@RequestBody @Valid ValidationActivite validationActivite,
                                     @PathVariable Long idValidationActivite,
                                     @RequestParam(name = "projet") Long idProjet) {
        validationActivite.setId(idValidationActivite);
        return validationActiviteService.update(validationActivite, idProjet);
    }

    @RequestMapping(value = "/validation-activites/{idValidationActivite}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long idValidationActivite,
                       @RequestParam(name = "projet") Long idProjet) {
        validationActiviteService.delete(idProjet,idValidationActivite);
    }

    @ApiOperation(value = "avoir les documents concernant une validation d'une activité", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/validation-activites/{idValidationActivite}/documents", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Document> getDocuments(@PathVariable Long idValidationActivite,
                                             @RequestParam(name = "projet") Long idProjet) {
        return validationActiviteService.getDocuments(idProjet,idValidationActivite);
    }
}