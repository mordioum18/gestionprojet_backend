package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Decaissement;
import sn.mesri.isepdiamniadio.gestionprojet.service.DecaissementService;

import javax.validation.Valid;
import java.util.Collection;

@Api("Decaissement")
@RestController
@RequestMapping(value = "/api")
public class DecaissementController {
    @Autowired
    private DecaissementService decaissementService;

    @RequestMapping(value = "/decaissements", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Decaissement> findAll(
            @SortDefault(sort = "libelle", direction = Sort.Direction.ASC)
            @PageableDefault(value = 5) Pageable pageable){
        return decaissementService.findAll(pageable);
    }

    @RequestMapping(value = "/decaissements", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Decaissement insert(@Valid @RequestBody Decaissement decaissement,
                                @RequestParam(name = "projet") Long idProjet) {
        return decaissementService.insert(idProjet, decaissement);
    }

    @RequestMapping(value = "/decaissements/{idDecaissement}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Decaissement findById(@PathVariable Long idDecaissement,
                                 @RequestParam(name = "projet") Long idProjet) {
        return decaissementService.findById(idProjet, idDecaissement);
    }

    @RequestMapping(value = "/decaissements/{idDecaissement}", method = RequestMethod.PUT)
    public Decaissement update(@RequestBody @Valid  Decaissement decaissement,
                               @PathVariable Long idDecaissement,
                               @RequestParam(name = "projet") Long idProjet) {
        decaissement.setId(idDecaissement);
        return decaissementService.update(idProjet, decaissement);
    }

    @RequestMapping(value = "/decaissements/{idDecaissement}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long idDecaissement,
                       @RequestParam(name = "projet") Long idProjet) {
        decaissementService.delete(idProjet, idDecaissement);
    }

}
