package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Document;
import sn.mesri.isepdiamniadio.gestionprojet.service.DocumentService;

import javax.validation.Valid;
import java.util.Collection;

@Api("Document")
@RestController
public class DocumentController {
    @Autowired
    private DocumentService documentService;

    @RequestMapping(value = "/documents", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Document> findAll(
            @SortDefault(sort = "id", direction = Sort.Direction.ASC)
            @PageableDefault(value = 5) Pageable pageable){
        return documentService.findAll(pageable);
    }

    @RequestMapping(value = "/documents", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Document insertDocument(@Valid @RequestBody Document document,
                                @RequestParam(name = "projet", required = true) Long idProjet) {
        return documentService.insertDocument(document, idProjet);
    }

    @RequestMapping(value = "/documents/{idDocument}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Document findDocumentById(@RequestParam(name = "projet", required = true) Long idProjet,@PathVariable Long idDocument) {
        return documentService.findDocumentById(idProjet,idDocument);
    }

    @RequestMapping(value = "/documents/{idDocument}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteDocument(@RequestParam(name = "projet", required = true) Long idProjet,@PathVariable Long idDocument) {
        documentService.deleteDocument(idProjet,idDocument);
    }
}
