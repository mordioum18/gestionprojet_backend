package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Projet;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Utilisateur;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.UtilisateurEvenement;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Visite;
import sn.mesri.isepdiamniadio.gestionprojet.service.VisiteService;

import javax.validation.Valid;
import java.util.Collection;

@Api("Visite")
@RestController
public class VisiteController {
    @Autowired
    private VisiteService visiteService;

    @RequestMapping(value = "/visites", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Visite> findAll(
            @SortDefault(sort = "titre", direction = Sort.Direction.ASC)
            @PageableDefault(value = 5) Pageable pageable){
        return visiteService.findAll(pageable);
    }

    @RequestMapping(value = "/visites/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Visite findById(@PathVariable Long id) {
        return visiteService.findById(id);
    }

    @RequestMapping(value = "/visites", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Visite insert(@Valid @RequestBody Visite visite,
                         @RequestParam(name = "user") Long idUtilisateur) {
        return visiteService.insert(visite, idUtilisateur);
    }

    @RequestMapping(value = "/visites/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Visite update(@RequestBody @Valid  Visite visite, @PathVariable Long id) {
        visite.setId(id);
        return visiteService.update(visite);
    }

    @RequestMapping(value = "/visites/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long id) {
        visiteService.delete(id);
    }

    @ApiOperation(value = "avoir les projets concernant une visite", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/visites/{id}/projets", method = RequestMethod.GET)
    public Collection<Projet> getProjets(@PathVariable Long id) {
        return visiteService.getProjets(id);
    }

    @RequestMapping(value = "/visites/{id}/utilisateurs", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Utilisateur> getUtilisateurs(@PathVariable Long id) {
        return visiteService.getUtilisateurs(id);
    }
}
