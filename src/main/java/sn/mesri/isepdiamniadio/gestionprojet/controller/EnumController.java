package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sn.mesri.isepdiamniadio.gestionprojet.entities.enums.RessourceType;
import sn.mesri.isepdiamniadio.gestionprojet.entities.enums.RessourceUnite;
import sn.mesri.isepdiamniadio.gestionprojet.entities.enums.Role;
import sn.mesri.isepdiamniadio.gestionprojet.entities.enums.Statut;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by hp on 17/11/2017.
 */

@Api("Enums")
@RestController
@RequestMapping(value = "/api")
public class EnumController {

    @ApiOperation(value = "Get all status", httpMethod = "GET")
    @RequestMapping(value = "/statuts", method = RequestMethod.GET)
    public Statut[] findAllStatuts() {
        return Statut.values();
    }

    @ApiOperation(value = "Get all roles", httpMethod = "GET")
    @RequestMapping(value = "/roles", method = RequestMethod.GET)
    public Role[] findAllRoles() {
        return Role.values();
    }

    @ApiOperation(value = "Get all ressources types", httpMethod = "GET")
    @RequestMapping(value = "/ressource-types", method = RequestMethod.GET)
    public RessourceType[] findAllRessourceTypes() {
        return RessourceType.values();
    }

    @ApiOperation(value = "Get all ressources unites", httpMethod = "GET")
    @RequestMapping(value = "/ressource-unites", method = RequestMethod.GET)
    public RessourceUnite[] findAllRessourceUnites() {
        return RessourceUnite.values();
    }

}
