package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Document;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.SuiviRessource;
import sn.mesri.isepdiamniadio.gestionprojet.service.SuiviRessourceService;

import javax.validation.Valid;
import java.util.Collection;

@Api("SuiviRessource")
@RestController
@RequestMapping(value = "/api")
public class SuiviRessourceController {
    @Autowired
    private SuiviRessourceService suiviRessourceService;

    @RequestMapping(value = "/suivi-ressources",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<SuiviRessource> findAll(
            @SortDefault(sort = "dateDemarrage", direction = Sort.Direction.ASC)
            @PageableDefault(value = 5) Pageable pageable){
        return suiviRessourceService.findAll(pageable);
    }

    @RequestMapping(value = "/suivi-ressources/{idSuiviRessource}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public SuiviRessource findById(@PathVariable Long idSuiviRessource,
                                   @RequestParam(name = "projet") Long idProjet) {
        return suiviRessourceService.findById(idProjet,idSuiviRessource);
    }

    @RequestMapping(value = "/suivi-ressources",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public SuiviRessource insert(@Valid @RequestBody SuiviRessource suiviRessource,
                                 @RequestParam(name = "projet") Long idProjet) {
        return suiviRessourceService.insert(suiviRessource,idProjet);
    }

    @RequestMapping(value = "/suivi-ressources/{idSuiviRessource}",method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public SuiviRessource update(@RequestBody @Valid SuiviRessource suiviRessource,
                                 @PathVariable Long idSuiviRessource,
                                 @RequestParam(name = "projet") Long idProjet) {
        suiviRessource.setId(idSuiviRessource);
        return suiviRessourceService.update(suiviRessource,idProjet);
    }

    @RequestMapping(value = "/suivi-ressources/{idSuiviRessource}",method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long idSuiviRessource,
                       @RequestParam(name = "projet") Long idProjet) {
        suiviRessourceService.delete(idProjet,idSuiviRessource);
    }
}
