package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Contrainte;
import sn.mesri.isepdiamniadio.gestionprojet.service.ContrainteService;
import javax.validation.Valid;
import java.util.Collection;

@Api("Contrainte")
@RestController
@RequestMapping(value = "/api")
public class ContrainteController {
    @Autowired
    private ContrainteService contrainteService;

    @RequestMapping(value = "/contraintes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Contrainte> findAll(
            @SortDefault(sort = "libelle", direction = Sort.Direction.ASC)
            @PageableDefault(value = 5) Pageable pageable){
        return contrainteService.findAll(pageable);
    }

    @RequestMapping(value = "/contraintes", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Contrainte insert(@Valid @RequestBody Contrainte contrainte,
                             @RequestParam(name = "projet", required = true) Long idProjet) {
        return contrainteService.insert(contrainte,idProjet);
    }

    @RequestMapping(value = "/contraintes/{idContrainte}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Contrainte update(@Valid @RequestBody Contrainte contrainte, @RequestParam(name = "projet", required = true) Long idProjet, @PathVariable Long idContrainte) {
                contrainte.setId(idContrainte);
        return contrainteService.update(contrainte,idProjet);
    }

    @RequestMapping(value = "/contraintes/{idContrainte}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Contrainte findById(@RequestParam(name = "projet", required = true) Long idProjet, @PathVariable Long idContrainte) {
        return contrainteService.findById(idProjet,idContrainte);
    }

    @RequestMapping(value = "/contraintes/{idContrainte}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@RequestParam(name = "projet", required = true) Long idProjet, @PathVariable Long idContrainte) {
        contrainteService.delete(idProjet, idContrainte);
    }

}
