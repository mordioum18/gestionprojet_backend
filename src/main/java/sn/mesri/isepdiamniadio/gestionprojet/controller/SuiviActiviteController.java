package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Document;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.SuiviActivite;
import sn.mesri.isepdiamniadio.gestionprojet.service.SuiviActiviteService;

import javax.validation.Valid;
import java.util.Collection;

@Api("SuiviActivité")
@RestController
@RequestMapping(value = "/api")
public class SuiviActiviteController {
    @Autowired
    private SuiviActiviteService suiviActiviteService;

    @RequestMapping(value ="/suivi-activites",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<SuiviActivite> findAll(
            @SortDefault(sort = "dateDemarrage", direction = Sort.Direction.ASC)
            @PageableDefault(value = 5) Pageable pageable){
        return suiviActiviteService.findAll(pageable);
    }

    @RequestMapping(value ="/suivi-activites/{idSuiviActivite}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public SuiviActivite findById(@PathVariable Long idSuiviActivite,
                                  @RequestParam(name = "projet") Long idProjet) {
        return suiviActiviteService.findById(idProjet,idSuiviActivite);
    }

    @RequestMapping(value = "/suivi-activites",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public SuiviActivite insert(@Valid @RequestBody SuiviActivite suiviActivite,
                                @RequestParam(name = "projet") Long idProjet) {
        return suiviActiviteService.insert(suiviActivite,idProjet);
    }

    @RequestMapping(value = "/suivi-activites/{idSuiviActivite}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long idSuiviActivite,
                       @RequestParam(name = "projet") Long idProjet) {
        suiviActiviteService.delete(idProjet,idSuiviActivite);
    }


    @ApiOperation(value = "avoir les documents concernant un suivi d'une activité", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/suivi-activites/{idSuiviActivite}/documents", method = RequestMethod.GET)
    public Collection<Document> getDocuments(@PathVariable Long idSuiviActivite,
                                             @RequestParam(name = "projet") Long idProjet) {
        return suiviActiviteService.getDocuments(idProjet,idSuiviActivite);
    }
}

