package sn.mesri.isepdiamniadio.gestionprojet.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Ressource;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.RessourceGroupe;
import sn.mesri.isepdiamniadio.gestionprojet.service.RessourceGroupeService;

import javax.validation.Valid;
import java.util.Collection;

@Api("RessourceGroupe")
@RestController
@RequestMapping(value = "/api")
public class RessourceGroupeController {
    @Autowired
    private RessourceGroupeService ressourceGroupeService;

    @RequestMapping(value = "/ressources-groupes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<RessourceGroupe> findAll(Pageable pageable) {
        return ressourceGroupeService.findAll(pageable);
    }

    @RequestMapping(value = "/ressources-groupes/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RessourceGroupe findById(Long id, Long idProjet) {
        return ressourceGroupeService.findById(id, idProjet);
    }

    @RequestMapping(value = "/ressources-groupes", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public RessourceGroupe insert(@Valid @RequestBody RessourceGroupe ressourceGroupe,
                                  @RequestParam(name = "projet") Long idProjet) {
        return ressourceGroupeService.insert(ressourceGroupe, idProjet);
    }

    @RequestMapping(value = "/ressources-groupes/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public RessourceGroupe update(@Valid @RequestBody RessourceGroupe ressourceGroupe,
                                  @PathVariable Long id,
                                  @RequestParam(name = "projet") Long idProjet) {
        ressourceGroupe.setId(id);
        return ressourceGroupeService.update(ressourceGroupe, idProjet);
    }

    @RequestMapping(value = "/ressources-groupes/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long id,
                       @RequestParam(name = "projet") Long idProjet) {
        ressourceGroupeService.delete(id, idProjet);
    }

    @ApiOperation(value = "avoir les ressources d'une équipe", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/ressources-groupes/{id}/ressources", method = RequestMethod.GET)
    public Collection<Ressource> getRessources(@PathVariable Long id,
                                               @RequestParam(name = "projet") Long idProjet) {
        return ressourceGroupeService.getRessources(id, idProjet);
    }
}
