package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Projet;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Reunion;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Utilisateur;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.UtilisateurEvenement;
import sn.mesri.isepdiamniadio.gestionprojet.service.ReunionService;

import javax.validation.Valid;
import java.util.Collection;

@Api("Reunion")
@RestController
public class ReunionController {
    @Autowired
    private ReunionService reunionService;

    @RequestMapping(value = "/reunions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Reunion> findAll(
            @SortDefault(sort = "titre", direction = Sort.Direction.ASC)
            @PageableDefault(value = 5) Pageable pageable){
        return reunionService.findAll(pageable);
    }

    @RequestMapping(value = "/reunions/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reunion findById(@PathVariable Long id) {
        return reunionService.findById(id);
    }

    @RequestMapping(value = "/reunions", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reunion insert(@Valid @RequestBody Reunion reunion) {
        return reunionService.insert(reunion);
    }

    @RequestMapping(value = "/reunions/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reunion update(@RequestBody @Valid  Reunion reunion, @PathVariable Long id) {
        reunion.setId(id);
        return reunionService.update(reunion);
    }

    @RequestMapping(value = "/reunions/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long id) {
        reunionService.delete(id);
    }

    @ApiOperation(value = "avoir les projets concernant une réunion", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/reunions/{id}/projets", method = RequestMethod.GET)
    public Collection<Projet> getProjets(@PathVariable Long id) {
        return reunionService.getProjets(id);
    }

    @RequestMapping(value = "/reunions/{id}/utilisateurs", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Utilisateur> getUtilisateurs(@PathVariable Long id) {
        return reunionService.getUtilisateurs(id);
    }
}
