package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;
import sn.mesri.isepdiamniadio.gestionprojet.service.LotService;

import javax.validation.Valid;
import java.util.Collection;

@Api("Lot")
@RestController
@RequestMapping(value = "/api")
public class LotController {
    @Autowired
    private LotService lotService;

    @RequestMapping(value = "/lots", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Lot> findAll(
            @SortDefault(sort = "libelle", direction = Sort.Direction.ASC)
            @PageableDefault(value = 5) Pageable pageable){
        return lotService.findAll(pageable);
    }

    @RequestMapping(value = "/lots", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Lot insert(@Valid @RequestBody Lot lot,
                      @RequestParam(name = "projet") Long idProjet) {
        return lotService.insert(lot, idProjet);
    }

    @RequestMapping(value = "/lots/{idLot}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Lot findById(@PathVariable Long idLot,
                        @RequestParam(name = "projet") Long idProjet) {
        return lotService.findById(idProjet,idLot);
    }

    @RequestMapping(value = "/lots/{idLot}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Lot update(@RequestBody @Valid  Lot lot, @PathVariable Long idLot,
                      @RequestParam(name = "projet") Long idProjet) {
        lot.setId(idLot);
        return lotService.update(lot,idProjet);
    }

    @RequestMapping(value = "/lots/{idLot}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long idLot,
                       @RequestParam(name = "projet") Long idProjet) {
        lotService.delete(idProjet,idLot);
    }

    @ApiOperation(value = "avoir les validations d'un lot", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/lots/{idLot}/validations", method = RequestMethod.GET)
    public Collection<ValidationLot> getValidationLots(@PathVariable Long idLot,
                                                       @RequestParam(name = "projet") Long idProjet) {
        return lotService.getValidationLots(idProjet,idLot);
    }

    @ApiOperation(value = "avoir les activités d'un lot", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/lots/{idLot}/activites", method = RequestMethod.GET)
    public Collection<Activite> getActivites(@PathVariable Long idLot,
                                             @RequestParam(name = "projet") Long idProjet) {
        return lotService.getActivites(idProjet,idLot);
    }

    @ApiOperation(value = "avoir les contraintes d'un lot", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/lots/{idLot}/contraintes", method = RequestMethod.GET)
    public Collection<Contrainte> getContraintes(@PathVariable Long idLot,
                                                 @RequestParam(name = "projet") Long idProjet) {
        return lotService.getContraintes(idProjet,idLot);
    }

    @ApiOperation(value = "Assign an organisation to a lot", httpMethod = "POST")
    @RequestMapping(value = "/lots/{id}/organisation", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public LotOrganisation assignOrganisationToLot(@PathVariable Long id,
                                                   @Valid @RequestBody LotOrganisation organisation,
                                                   @RequestParam(name = "projet") Long idProjet) {
        Lot lot = lotService.findById(idProjet, id);
        return lotService.assignOrganisationToLot(lot, organisation, idProjet);
    }

    @RequestMapping(value = "/lots/{id}/lots-organisations", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<LotOrganisation> getLotOrganisations(@PathVariable Long id,
                                                           @RequestParam(name = "projet") Long idProjet) {
        return lotService.getLotOrganisations(id, idProjet);
    }

    @RequestMapping(value = "/lots/{id}/organisations", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Organisation> getOrganisations(@PathVariable Long id,
                                                     @RequestParam(name = "projet") Long idProjet) {
        return lotService.getOrganisations(id, idProjet);
    }
}
