package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;
import sn.mesri.isepdiamniadio.gestionprojet.service.ActiviteService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@Api("Activité")
@RestController
@RequestMapping(value = "/api")
public class ActiviteController {
    @Autowired
    private ActiviteService activiteService;

    @ApiOperation(value = "créer une activité", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/activites", method = RequestMethod.POST)
    public Activite insert(@Valid @RequestBody Activite activite,
                           @RequestParam(name = "projet") Long idProjet) {
        return activiteService.insert(activite, idProjet);
    }

    @ApiOperation(value = "avoir une activité", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/activites/{idActivite}", method = RequestMethod.GET)
    public Activite findById(@RequestParam(name = "projet") Long idProjet,
                             @PathVariable Long idActivite) {
        return activiteService.findById(idProjet, idActivite);
    }

    @ApiOperation(value = "avoir toutes les activités", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/activites", method = RequestMethod.GET)
    public Page<Activite> findAll(
            @SortDefault(sort = "libelle", direction = Sort.Direction.ASC)
            @PageableDefault(value = 5) Pageable pageable){
        return activiteService.findAll(pageable);
    }

    @ApiOperation(value = "modifier une activité", httpMethod = "PUT", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/activites/{idActivite}", method = RequestMethod.PUT)
    public Activite update(@Valid @RequestBody Activite activite,
                           @RequestParam(name = "projet") Long idProjet,
                           @PathVariable Long idActivite) {
        activite.setId(idActivite);
        return activiteService.update(activite, idProjet);
    }

    @ApiOperation(value = "supprimer une activité", httpMethod = "DELETE", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/activites/{idActivite}", method = RequestMethod.DELETE)
    public void delete(@RequestParam(name = "projet") Long idProjet, @PathVariable Long idActivite) {
        activiteService.delete(idProjet,idActivite);
    }

    @ApiOperation(value = "avoir les suivis d'une activité", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/activites/{idActivite}/suivis", method = RequestMethod.GET)
    public Collection<SuiviActivite> getSuiviActivites(@RequestParam(name = "projet") Long idProjet, @PathVariable Long idActivite) {
        return activiteService.getSuiviActivites(idProjet, idActivite);
    }

    @ApiOperation(value = "avoir les validations d'une activité", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/activites/{idActivite}/validations", method = RequestMethod.GET)
    public Collection<ValidationActivite> getValidationActivites(@RequestParam(name = "projet") Long idProjet, @PathVariable Long idActivite) {
        return activiteService.getValidationActivites(idProjet, idActivite);
    }

    @ApiOperation(value = "avoir les contraintes d'une activité", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/activites/{idActivite}/contraintes", method = RequestMethod.GET)
    public Collection<Contrainte> getContraintes(@RequestParam(name = "projet") Long idProjet, @PathVariable Long idActivite) {
        return activiteService.getContraintes(idProjet,idActivite);
    }

    @ApiOperation(value = "avoir les sous-activités d'une activité", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/activites/{idActivite}/activites", method = RequestMethod.GET)
    public Collection<Activite> getActivitesFilles(@RequestParam(name = "projet") Long idProjet, @PathVariable Long idActivite) {
        return activiteService.getActivitesFilles(idProjet,idActivite);
    }

    @ApiOperation(value = "créer un lien entre taches", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/activites/liens", method = RequestMethod.POST)
    public LienTache addLink(@RequestParam(name = "projet", required = true) Long idProjet,
                             @RequestParam(name = "predecesseur", required = true) Long idPredecesseur,
                             @RequestParam(name = "successeur", required = true) Long idSuccesseur,
                             @RequestParam(name = "type", required = true) String type) {
        return activiteService.addLink(idProjet, idPredecesseur, idSuccesseur, type);
    }

    @ApiOperation(value = "supprimer un lien entre taches", httpMethod = "DELETE", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/activites/liens", method = RequestMethod.DELETE)
    public void deleteLink(@RequestParam(name = "projet", required = true) Long idProjet,
                             @RequestParam(name = "predecesseur", required = true) Long idPredecesseur,
                             @RequestParam(name = "successeur", required = true) Long idSuccesseur) {
         activiteService.deleteLink(idProjet, idPredecesseur, idSuccesseur);
    }

    @ApiOperation(value = "avoir les décaissement d'une activité", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/activites/{idActivite}/decaissements", method = RequestMethod.GET)
    public Collection<Decaissement> getDecaissements(@RequestParam(name = "projet") Long idProjet, @PathVariable Long idActivite) {
        return activiteService.getDecaissements(idProjet,idActivite);
    }

    @ApiOperation(value = "tous les succesurs d'une activité", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/activites/{idActivite}/successeurs", method = RequestMethod.GET)
    public Collection<Activite> getSuccesseurs(@RequestParam(name = "projet") Long idProjet, @PathVariable Long idActivite) {
        return activiteService.getSuccesseurs(idProjet,idActivite);
    }

    @ApiOperation(value = "Affecter une ressource à une activité", httpMethod = "POST")
    @RequestMapping(value = "/activites/{id}/ressource", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ActiviteRessource assignRessourceToActivite(@PathVariable Long id,
                                                       @Valid @RequestBody ActiviteRessource ressource,
                                                       @RequestParam(name = "projet") Long idProjet) {
        Activite activite = activiteService.findById(idProjet, id);
        return activiteService.assignRessourceToActivite(activite, ressource, idProjet);
    }

    @ApiOperation(value = "Modifier une affectation de ressource à une activité", httpMethod = "POST")
    @RequestMapping(value = "/activites/{id}/ressource", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ActiviteRessource updateAssignRessourceToActivite(@PathVariable Long id,
                                                             @Valid @RequestBody ActiviteRessource ressource,
                                                             @RequestParam(name = "projet") Long idProjet) {
        Activite activite = activiteService.findById(idProjet, id);
        return activiteService.updateAssignRessourceToActivite(activite, ressource, idProjet);
    }

    @RequestMapping(value = "/activites/{id}/activite-ressources", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<ActiviteRessource> getActiviteRessources(@PathVariable Long id,
                                                               @RequestParam(name = "projet") Long idProjet) {
        return activiteService.getActiviteRessources(id, idProjet);
    }

    @RequestMapping(value = "/activites/{id}/ressources", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Ressource> getRessources(@PathVariable Long id,
                                               @RequestParam(name = "projet") Long idProjet) {
        return activiteService.getRessources(id, idProjet);
    }
}
