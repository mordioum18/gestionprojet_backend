package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;
import sn.mesri.isepdiamniadio.gestionprojet.service.ProjetService;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.Collection;
import java.util.List;

@Api("Projet")
@RestController
@RequestMapping(value = "/api")
public class ProjetController {
    @Autowired
    private ProjetService projetService;

    @RequestMapping(value = "/projets/nom",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Projet> findByName(@RequestParam(value = "nom") String nom) {
        return projetService.findByNom(nom);
    }

    @RequestMapping(value = "/projets",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Projet> findAll(
            @SortDefault(sort = "nom", direction = Sort.Direction.ASC)
            @PageableDefault(value = 10) Pageable pageable){
        return projetService.findAll(pageable);
    }
    @RequestMapping(value = "/projets/{projet_id}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    //@PreAuthorize("hasPermission(#projet_id, 'Projet', 'ROLE_PROJET_CHEF_DE_PROJET')")
    public Projet findById(@PathVariable Long projet_id){
        //Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return projetService.findById(projet_id);
    }
    @RequestMapping(value = "/projets/{projet_id}",method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    //@PreAuthorize("hasPermission(#id, 'Projet', 'ADMIN')")
    public void delete(@PathVariable Long projet_id){
        projetService.delete(projet_id);
    }

    @RequestMapping(value = "/projets",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Projet insert(@RequestBody @Valid Projet projet){
        return projetService.insert(projet);
    }
    @RequestMapping(value = "/projets/{projet_id}",method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    // @PreAuthorize("hasPermission(#type_id, 'Projet', 'ROLE_PROJET_CHEF_DE_PROJET')")
    public Projet update(@PathVariable Long projet_id, @RequestBody @Valid Projet projet){

        projet.setId(projet_id);
        return projetService.update(projet);
    }
    @RequestMapping(value = "/projets/{projet_id}/acteurs",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<UtilisateurProjet> listActeurs(@PathVariable Long projet_id){
        return projetService.listActeurs(projet_id);
    }
    @RequestMapping(value = "/projets/{projet_id}/acteurs",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public UtilisateurProjet addUtilisateurRole(
             @PathVariable Long projet_id,
             @RequestBody @Valid UtilisateurProjet utilisateurProjet){
         return projetService.addUtilisateurRole(projet_id, utilisateurProjet);
    }
    @RequestMapping(value = "/projets/{projet_id}/acteurs",method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteUtilisateurProjet(
             @PathVariable Long projet_id,
             @RequestParam(name = "utilisateur", required = true) Long idUtilisateur){
        projetService.deleteUtilisateurProjet(projet_id,idUtilisateur);
    }


    @RequestMapping(value = "/projets/{idProjet}/lots",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Lot> getLot(@PathVariable Long idProjet){

                 return projetService.getLot(idProjet);
    }

    @RequestMapping(value = "/projets/{idProjet}/activites",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Activite> getActivite(@PathVariable Long idProjet){

        return projetService.getActivite(idProjet);
    }

    @ApiOperation(value = "tous les lies des taches", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/projets/{idProjet}/liens", method = RequestMethod.GET)
    public Collection<LienTache> getLiens(@PathVariable Long idProjet) {
        return projetService.getLiens(idProjet);
    }

    @ApiOperation(value = "touts les financements d'un projet", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/projets/{idProjet}/financements", method = RequestMethod.GET)
    public Collection<Financement> getFinancements(@PathVariable Long idProjet) {
        return projetService.getFinancements(idProjet);
    }

    @ApiOperation(value = "avoir les ressources d'un projet", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/projets/{id}/ressources", method = RequestMethod.GET)
    public Collection<Ressource> getRessources(@PathVariable Long id) {
        return projetService.getRessources(id);
    }

    @ApiOperation(value = "avoir les groupes de ressources d'un projet", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/projets/{id}/ressource-groupes", method = RequestMethod.GET)
    public Collection<RessourceGroupe> getRessourceGroupes(@PathVariable Long id) {
        return projetService.getRessourceGroupes(id);
    }
}
