package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Activite;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.ActiviteRessource;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Ressource;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.SuiviRessource;
import sn.mesri.isepdiamniadio.gestionprojet.service.RessourceService;

import javax.validation.Valid;
import java.util.Collection;

@Api("Ressource")
@RestController
@RequestMapping(value = "/api")
public class RessourceController {
    @Autowired
    private RessourceService ressourceService;

    @RequestMapping(value = "/ressources", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Ressource> findAll(@SortDefault(sort = "libelle", direction = Sort.Direction.ASC)
                                   @PageableDefault(value = 5) Pageable pageable){
        return ressourceService.findAll(pageable);
    }

    @RequestMapping(value = "/ressources/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Ressource findById(@PathVariable Long id,
                              @RequestParam(name = "projet") Long idProjet) {
        return ressourceService.findById(id, idProjet);
    }


    @RequestMapping(value = "/ressources", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Ressource insert(@Valid @RequestBody Ressource ressource,
                            @RequestParam(name = "projet") Long idProjet) {
        return ressourceService.insert(ressource, idProjet);
    }

    @RequestMapping(value = "/ressources/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Ressource update(@Valid @RequestBody Ressource ressource,
                            @PathVariable Long id,
                            @RequestParam(name = "projet") Long idProjet) {
        ressource.setId(id);
        return ressourceService.update(ressource, idProjet);
    }

    @RequestMapping(value = "/ressources/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long id,
                       @RequestParam(name = "projet") Long idProjet) {
        ressourceService.delete(id, idProjet);
    }

    @RequestMapping(value = "/ressources/{id}/activite-ressources", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<ActiviteRessource> getActiviteRessources(@PathVariable Long id,
                                                               @RequestParam(name = "projet") Long idProjet) {
        return ressourceService.getActiviteRessources(id, idProjet);
    }

    @RequestMapping(value = "/ressources/{id}/activites", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Activite> getActivites(@PathVariable Long id,
                                             @RequestParam(name = "projet") Long idProjet) {
        return ressourceService.getActivites(id, idProjet);
    }

    @RequestMapping(value = "/ressources/{id}/suivis", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<SuiviRessource> getSuiviRessources(@PathVariable Long id,
                                                         @RequestParam(name = "projet") Long idProjet) {
        return ressourceService.getSuiviRessources(id, idProjet);
    }
}
