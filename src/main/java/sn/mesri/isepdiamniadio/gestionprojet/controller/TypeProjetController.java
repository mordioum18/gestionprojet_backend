package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Projet;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.TypeProjet;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.TypeProjet;
import sn.mesri.isepdiamniadio.gestionprojet.service.TypeProjetService;
import sn.mesri.isepdiamniadio.gestionprojet.service.TypeProjetService;

import javax.validation.Valid;
import java.util.Collection;

@Api("TypeProjet")
@RestController
@RequestMapping(value = "/api")
public class TypeProjetController {
    @Autowired
    private TypeProjetService typeProjetService;

    @RequestMapping(value = "/type-projets", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<TypeProjet> findAll(){
        return typeProjetService.findAll();
    }

    @RequestMapping(value = "/type-projets/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public TypeProjet findById(@PathVariable("id") Long id) {
        return typeProjetService.findById(id);
    }

    @RequestMapping(value = "/type-projets", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public TypeProjet insert(@RequestBody @Valid TypeProjet typeProjet) {
        return typeProjetService.insert(typeProjet);
    }

    @RequestMapping(value = "/type-projets/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public TypeProjet update(@RequestBody @Valid  TypeProjet typeProjet, @PathVariable Long id) {
        typeProjet.setId(id);
        return typeProjetService.update(typeProjet);
    }

    @RequestMapping(value = "/type-projets/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long id) {
        typeProjetService.delete(id);
    }

    @ApiOperation(value = "avoir les projets pour un type de projet donné", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/type-projets/{id}/projets", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Projet> getProjets(@PathVariable Long id) {
        return typeProjetService.getProjets(id);
    }
}

