package sn.mesri.isepdiamniadio.gestionprojet.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Lot;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.LotOrganisation;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Organisation;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Utilisateur;
import sn.mesri.isepdiamniadio.gestionprojet.service.OrganisationService;

import javax.validation.Valid;
import java.util.Collection;

@Api("Organisation")
@RestController
@RequestMapping(value = "/api")
public class OrganisationController {
    @Autowired
    private OrganisationService organisationService;

    @RequestMapping(value = "/organisations", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Organisation> findAll(){
        return organisationService.findAll();
    }

    @RequestMapping(value = "/organisations/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Organisation findById(@PathVariable Long id) {
        return organisationService.findById(id);
    }

    @RequestMapping(value = "/organisations", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Organisation insert(@RequestBody @Valid Organisation organisation) {
        return organisationService.insert(organisation);
    }

    @RequestMapping(value = "/organisations/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Organisation update(@RequestBody @Valid  Organisation organisation, @PathVariable Long id) {
        organisation.setId(id);
        return organisationService.update(organisation);
    }

    @RequestMapping(value = "/organisations/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long id) {
        organisationService.delete(id);
    }

    @ApiOperation(value = "avoir les utilisateurs d'une organisation", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/organisations/{id}/utilisateurs", method = RequestMethod.GET)
    public Collection<Utilisateur> getUtilisateurs(@PathVariable Long id) {
        return organisationService.getUtilisateurs(id);
    }
}
