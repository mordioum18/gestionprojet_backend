package sn.mesri.isepdiamniadio.gestionprojet.entities.enums;

public enum Profil implements EnumType<String>  {
    SUPERADMIN("SUPERADMIN","SUPER_ADMIN"),
    CREATEURPROJET("CREATEURPROJET","CREATEUR_PROJET"),
    ACTEURPROJET("ACTEURPROJET","ACTEUR_PROJET"),
    REPRESENTANT("REPRESENTANT","REPRESENTANT");

    private String id;
    private String name;

    Profil(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName(){
        return name;
    }

    @Override
    public String getId() {
        return id;
    }

}
