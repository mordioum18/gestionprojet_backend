package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Data
@Embeddable
public class LienTachePk implements Serializable {
    @ManyToOne
    @JoinColumn(name = "predecesseur_id")
    private Activite predecesseur;
    @ManyToOne
    @JoinColumn(name = "successeur_id")
    private Activite successeur;
}
