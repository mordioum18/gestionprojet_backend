package sn.mesri.isepdiamniadio.gestionprojet.entities.enums;

public enum  LinkTacheType implements EnumType<String>{
    FINDEBUT("FD","Fin à Début"),
    DEBUTDEBUT("DD","Début à Début"),
    FINFIN("FF","Fin à Fin"),
    DEBUTFIN("DF","Début à Fin");

    private String id;
    private String name;

    LinkTacheType(String id, String name) {
        this.id = id;
        this.name = name;
    }


    public String getName(){
        return name;
    }

    @Override
    public String getId() {
        return id;
    }

}
