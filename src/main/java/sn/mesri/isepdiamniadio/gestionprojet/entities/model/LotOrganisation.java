package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class LotOrganisation {
    @EmbeddedId
    private LotOrganisationPk pk;
    private Date dateDebutAttribution;
    private Date dateFinAttribution;

}
