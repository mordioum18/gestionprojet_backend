package sn.mesri.isepdiamniadio.gestionprojet.entities.model;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.util.Collection;

@Entity
@Data
public class Reunion extends Evenement{
    @ElementCollection
    @Column(columnDefinition = "text")
    private Collection<String> ordreDuJour;
    @ElementCollection
    @Column(columnDefinition = "text")
    private Collection<String> pointsAbordes;
    @ElementCollection
    @Column(columnDefinition = "text")
    private Collection<String> actions;


}
