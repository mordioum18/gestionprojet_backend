package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
public class Contrainte implements Serializable{
    @Id
    @GeneratedValue
    private Long id;
    private Date dateCreation;
    private Date dateModification;
    @NotBlank(message = "le libelle ne peut pas être vide")
    private String libelle;
    @Column(columnDefinition = "text")
    private String description;
    @ManyToOne
    @JoinColumn(name = "activite_id")
    private Activite activite;
    @ManyToOne
    @JoinColumn(name = "lot_id") // contraintes concernant le lot de maniere generale
    private Lot lot;

}
