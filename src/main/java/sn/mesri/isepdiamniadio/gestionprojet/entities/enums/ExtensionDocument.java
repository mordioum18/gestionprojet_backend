package sn.mesri.isepdiamniadio.gestionprojet.entities.enums;

public enum ExtensionDocument implements EnumType<String> {
    PDF("PDF","PDF"),
    PNG("PNG","PNG"),
    JPG("JPG","JPG"),
    pdf("pdf","pdf"),
    jpg("jpg","jpg"),
    png("png","png");

    private String id;
    private String name;

    ExtensionDocument(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName(){
        return name;
    }

    @Override
    public String getId() {
        return id;
    }


    public static Boolean findByName(String name) {
        for(ExtensionDocument extensionDocument : values()) {
            if(extensionDocument.name.equals(name))
                return true;
        }
        return  false;
    }

}
