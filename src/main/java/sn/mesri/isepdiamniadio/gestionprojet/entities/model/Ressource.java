package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.Collection;
import java.util.Date;

@Entity
@Data
public class Ressource {
    @Id
    @GeneratedValue
    private Long id;
    private Date dateCreation;
    private Date dateModification;
    private String nom;
    private String typeRessource;
    @ManyToOne
    @JoinColumn(name = "ressource_groupe_id")
    private RessourceGroupe ressourceGroupe;
    private String unite;
    private Double quantitePrevue;
    @NotBlank(message = "l'email ne peut pas être vide")
    @Pattern(regexp = "^([a-z]|[0-9]|[A-Z]|[_-]|.)+@([a-z]|[0-9]|[A-Z]|[_-]|.)+.[a-z]{2,3}$",message = "email incorrect")
    private String email;
    // Ajouter des ressources à un projet
    /* Les ressources sont généralement des personnes(ou matériel) incluses dans votre plan de projet,
    qu’elles soient affectées aux tâches ou non */
    @ManyToOne
    @JoinColumn(name = "projet_id")
    private Projet projet;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pk.ressource", fetch = FetchType.LAZY)
    private Collection<ActiviteRessource> activiteRessources;
    @OrderBy("dateSuivi DESC")
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ressource", fetch = FetchType.LAZY)
    private Collection<SuiviRessource> suiviRessources;

      /* disponibilite au plutot = min (date de debut au plutot des taches affectees)
       disponibilite au plus tard = min (date de debut au plus tard des taches affectees)
    */
   /* private Date disponibiliteAuPlusTot;
    private Date disponibiliteAuPlusTard;*/

    // nombre d’unités total que cette ressource est disponible pour ce projet
    /* La valeur d’unités d’affectation indique la quantité de cette ressource est disponible pour ce projet,
    par exemple, à temps partiel ou multiples.*/
    // On utilise la capacité maximale pour spécifier plusieurs disponibilité d’une ressource
    // On peut spécifier la capacité maximale en pourcentage (50 %, 100 %, 300 %), ou en décimales (0,5, 1, 3).
}
