package sn.mesri.isepdiamniadio.gestionprojet.entities.enums;

public enum RessourceType implements EnumType<String> {
    RH("RH","Ressource Humaine"),
    Mat("Mat","Matériaux"),
    RM("RM","Ressource Matérielle");

    private String id;
    private String name;

    RessourceType(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName(){
        return name;
    }

    @Override
    public String getId() {
        return id;
    }


    public static Boolean findByName(String name) {
        for(RessourceType ressourceType : values()) {
            if(ressourceType.name.equals(name))
                return true;
        }
        return  false;
    }

}
