package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Data
@Entity
public class TypeProjet implements Serializable{
    @Id
    @GeneratedValue
    private Long id;
    private Date dateCreation;
    private Date dateModification;
    @NotBlank(message = "le libbellé ne peut pas être vide")
    @Length(max = 255, message = "libellé très long")
    private String libelle;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "typeProjet", fetch = FetchType.LAZY)
    private Collection<Projet> projets;

}
