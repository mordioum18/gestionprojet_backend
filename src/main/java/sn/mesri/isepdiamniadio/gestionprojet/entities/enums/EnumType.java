package sn.mesri.isepdiamniadio.gestionprojet.entities.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public interface EnumType<IdType> {
    IdType getId();
}
