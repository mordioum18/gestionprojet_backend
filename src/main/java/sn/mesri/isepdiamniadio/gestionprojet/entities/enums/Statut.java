package sn.mesri.isepdiamniadio.gestionprojet.entities.enums;

import sn.mesri.isepdiamniadio.gestionprojet.entities.enums.EnumType;

public enum Statut implements EnumType<String> {
    TODO("TODO","Nouveau"),
    DOING("DOING","En cours"),
    DONE("DONE","Terminé"),
    STOP("STOP","Arrêté");

    private String id;
    private String name;

    Statut(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName(){
        return name;
    }

    @Override
    public String getId() {
        return id;
    }

    public static Boolean findByName(String name) {
        for(Statut statut : values()) {
            if(statut.name.equals(name))
                return true;
        }
        return  false;
    }
}
