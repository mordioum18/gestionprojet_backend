package sn.mesri.isepdiamniadio.gestionprojet.entities.enums;

public enum Role implements EnumType<String> {
    ADMINPROJET("ADMINPROJET","ADMINISTRATEUR_PROJET"),
    CHEFPROJET("CHEFPROJET","CHEF_DE_PROJET");

    private String id;
    private String name;

    Role(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName(){
        return name;
    }

    @Override
    public String getId() {
        return id;
    }


    public static Role findById(String id) {
        for(Role role : values()) {
            if(role.id.equals(id))
                return role;
        }
        return  null;
    }
    public static Boolean findByName(String name) {
        for(Role role : values()) {
            if(role.name.equals(name))
                return true;
        }
        return  false;
    }

}
