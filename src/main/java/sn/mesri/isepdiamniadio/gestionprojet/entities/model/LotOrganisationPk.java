package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Data
@Embeddable
public class LotOrganisationPk implements Serializable {
    @ManyToOne
    private Lot lot;
    @ManyToOne
    private Organisation organisation;
}
