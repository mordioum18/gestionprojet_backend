package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Data
@Embeddable
public class ActiviteRessourcePk implements Serializable {
    @ManyToOne
    private Activite activite;
    @ManyToOne
    private Ressource ressource;
   /* @ManyToOne
    private Utilisateur responsable;*/
}
