package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Suivi implements Serializable{
    @Id
    @GeneratedValue
    protected Long id;
    protected Date dateCreation;
    protected Date dateModification;
    protected Date dateSuivi;
    @Column(columnDefinition = "text")
    protected String observation;
    @ManyToOne
    @JoinColumn(name = "evenement_id")
    protected Evenement evenement;
    @ManyToOne
    @JoinColumn(name = "utilisateur_id")
    protected Utilisateur utilisateur;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "suivi",fetch = FetchType.EAGER)
    protected Collection<Document> documents;

}
