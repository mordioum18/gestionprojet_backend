package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@Entity
public class ValidationLot extends Validation{
    @ManyToOne
    @JoinColumn(name = "lot_id")
    private Lot lot;
}
