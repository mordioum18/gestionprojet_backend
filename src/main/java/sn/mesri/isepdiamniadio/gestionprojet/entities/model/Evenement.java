package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Data
public abstract class Evenement implements Serializable {
    @Id
    @GeneratedValue
    protected Long id;
    protected Date dateDebut;
    protected Date dateFin;
    protected String titre;
    protected String description;
    protected Date dateCreation;
    protected Date dateModification;
    // utilisateur qui cree l evenement
    @ManyToOne
    @JoinColumn(name = "utilisateur_id")
    protected Utilisateur utilisateur;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pk.evenement", fetch = FetchType.LAZY)
    private Collection<UtilisateurEvenement> utilisateurEvenements;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evenement", fetch = FetchType.LAZY)
    private Collection<Validation> validations;
    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL, mappedBy="evenements")
    protected Collection<Projet> projets;

}
