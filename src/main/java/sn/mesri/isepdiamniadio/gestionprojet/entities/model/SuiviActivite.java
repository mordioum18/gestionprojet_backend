package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Data
@Entity
public class SuiviActivite extends Suivi {
    private Long retard;
    private int niveauRealisation;
    @ManyToOne
    @JoinColumn(name = "activite_id")
    private Activite activite;
}
