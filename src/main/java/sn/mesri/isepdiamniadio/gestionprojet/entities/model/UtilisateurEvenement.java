package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Data
@Entity
public class UtilisateurEvenement {
    @EmbeddedId
    private UtilisateurEvenementPk pk;
    @Column(columnDefinition = "boolean Default false")
    private Boolean etat;
}
