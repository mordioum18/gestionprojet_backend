package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Data
@Embeddable
public class UtilisateurEvenementPk implements Serializable {
    @ManyToOne
    private Utilisateur utilisateur;
    @ManyToOne
    private Evenement evenement;

}
