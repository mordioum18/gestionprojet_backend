package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Data
@Entity
public class LienTache {
    @EmbeddedId
    private LienTachePk pk;
    // type de lien entre les deux taches
    private String codeType;
    private String libelleType;
}
