package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Entity
@Data
public class Projet implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    @NotBlank(message = "le nom du projet ne peut pas être vide")
    @Length(max = 255, message = "nom très long")
    private String nom;
    private Double coutTotal;
    @Column(columnDefinition = "text")
    private String description;
    private Date dateExecutionFinanciere;
    private Date dateDemarrageReelle;
    @NotNull(message = "la date de démarrage du projet ne peut pas être vide")
    private Date dateDemarragePrevisionnelle;
    private Date dateFinReelle;
    private Date dateFinPrevisionnelle;
    private int dureeExecutionPrevisionnelle; //qui renseigne la dateFinExecutionPrevisionnelle;
    private int dureeExecutionReelle; // qui renseigne la dateFinExecutionReelle
    @NotNull(message = "la date de réception du projet ne peut pas être vide")
    private Date dateReceptionPrevisionnelle;
    private Date dateReceptionReelle;
    private Date dateCreation;
    private Date dateModification;
    @Length(max = 255, message = "statut très long")
    private String statutProjet;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projetParent",fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<Projet> projets;
    @ManyToOne
    @JoinColumn(name = "projet_parent_id")
    private Projet projetParent;
    @ManyToOne
    @JoinColumn(name = "type_projet_id")
    private TypeProjet typeProjet;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projet",fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<Financement> financements;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval=true, mappedBy = "pk.projet")
    @JsonIgnore
    private Collection<UtilisateurProjet> utilisateurProjets;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projet",fetch = FetchType.LAZY)
    private Collection<Lot> lots;
    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name="projet_evenement",
            joinColumns=@JoinColumn(name="projet_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="evenement_id", referencedColumnName="id"))
    private Collection<Evenement> evenements;

    // utilisateur qui cree le projet
    @ManyToOne
    @JoinColumn(name = "utilisateur_id")
    private Utilisateur createur;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projet",fetch = FetchType.LAZY)
    private Collection<Ressource> ressources;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projet",fetch = FetchType.LAZY)
    private Collection<RessourceGroupe>  ressourceGroupes;


}
