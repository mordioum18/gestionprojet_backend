package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

@Data
@Entity
public class Decaissement implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    @NotBlank(message = "le libelle ne peut pas être vide")
    private String libelle;
    @Column(columnDefinition = "text")
    private String description;
    private Date dateCreation;
    private Date dateModification;
    @NotNull(message = "le montant ne peut pas être vide")
    private Double montant;
    private Date dateDecaissement;
    @ManyToOne
    @JoinColumn(name = "utilisateur_id")
    private Utilisateur utilisateur;
    @ManyToOne
    @JoinColumn(name = "activite_id")
    private Activite activite;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "decaissement")
    protected Collection<Document> documents;

}
