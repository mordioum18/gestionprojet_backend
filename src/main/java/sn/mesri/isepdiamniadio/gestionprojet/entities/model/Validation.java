package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Validation implements Serializable{
    @Id
    @GeneratedValue
    protected Long id;
    protected Date dateCreation;
    protected Date dateModification;
    protected Date dateValidation;
    @Column(columnDefinition = "text")
    @NotBlank(message = "le vommentaire ne peut pas être vide")
    protected String commentaire;
    @ManyToOne
    @JoinColumn(name = "evenement_id")
    protected Evenement evenement;
    @ManyToOne
    @JoinColumn(name = "utilisateur_id")
    protected Utilisateur utilisateur;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "validation",fetch = FetchType.LAZY)
    protected Collection<Document> documents;
}
