package sn.mesri.isepdiamniadio.gestionprojet.entities.enums;

public enum RessourceUnite implements EnumType<String> {
    SAC("SAC","sac"),
    ROULEAU("ROULEAU","rouleau"),
    M3("M3","m3"),
    TONNE("TONNE","tonne"),
    LITRE("LITRE","litre"),
    KG("KG","kg"),
    UNITE("UNITE","unité"),
    U("U","u");

    private String id;
    private String name;

    RessourceUnite(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName(){
        return name;
    }

    @Override
    public String getId() {
        return id;
    }

    public static Boolean findByName(String name) {
        for(RessourceUnite ressourceUnite : values()) {
            if(ressourceUnite.name.equals(name))
                return true;
        }
        return  false;
    }
}
