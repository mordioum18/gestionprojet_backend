package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Data
public class Visite extends Evenement{
    @Length(max = 255, message = "motif très long")
    private String motif;
    @Column(columnDefinition = "text")
    private String commentaire;

}
