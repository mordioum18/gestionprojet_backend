package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Data
public class RessourceGroupe {
    @Id
    @GeneratedValue
    private Long id;
    private String nom;
    @ManyToOne
    @JoinColumn(name = "projet_id")
    private Projet projet;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ressourceGroupe",fetch = FetchType.LAZY)
    Collection<Ressource> ressources;
    private Date dateCreation;
    private Date dateModification;
}
