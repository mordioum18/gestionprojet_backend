package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

@Data
@Entity
public class ActiviteRessource {
    @EmbeddedId
    private ActiviteRessourcePk pk;
    private Date dateAssignation;
    private Date dateModification;
    private String roleDansLaTache;
    private Double quantite;
    private Double coutParUnite;
    @ManyToOne
    @JoinColumn(name = "responsable_id")
    private Utilisateur responsable;
    // private String typeReservation;
}
