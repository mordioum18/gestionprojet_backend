package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Collection;
import java.util.Date;

@Entity
@Data
public class Utilisateur {
    @Id
    @GeneratedValue
    private Long id;
    @NotNull(message = "le prénom ne peut pas être vide")
    private String prenom;
    @NotNull(message = "le nom ne peut pas être vide")
    private String nom;
    @Length(max = 255,message = "numéro téléphone très long")
    @Column(unique = true, nullable = true)
    private String telephone;
    @NotBlank(message = "l'email ne peut pas être vide")
    @Pattern(regexp = "^([a-z]|[0-9]|[A-Z]|[_-]|.)+@([a-z]|[0-9]|[A-Z]|[_-]|.)+.[a-z]{2,3}$",message = "email incorrect")
    @Column(unique = true)
    private String email;
    @NotBlank(message = "le login ne peut pas être vide")
    @Length(max = 255,message = "login très long")
    @Column(unique = true)
    private String username;
    @JsonIgnore
    private String password;
    @Length(max = 255,message = "mot de passe très long")
    @Transient
    private String passwordPlain;
    private Date dateCreation;
    private Date dateModification;
    @JsonIgnore
    @Column(columnDefinition = "boolean Default false")
    private Boolean actif;
    // le profil de l'utilisateur dans l'application
    private String profil;
    // liste des projets dont l utilisateur est un acteur avec son role
    @OneToMany(cascade = CascadeType.ALL, mappedBy="pk.utilisateur",fetch = FetchType.EAGER)
    @JsonIgnore
    private Collection<UtilisateurProjet> utilisateurProjets;

    // liste des evenements dont l utilsateur a participe
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pk.utilisateur", fetch = FetchType.LAZY)
    private Collection<UtilisateurEvenement> utilisateurEvenements;

    // liste des evenements crees par l utilisateur
    @OneToMany(cascade = CascadeType.ALL, mappedBy="utilisateur")
    @JsonIgnore
    private Collection<Evenement> evenementOrganises;

    @ManyToOne
    @JoinColumn(name = "organisation_id")
    private Organisation organisation;

    // liste de projets crees par un utilisateur
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createur", fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<Projet> projets;

    // liste des ActiviteRessource dont l'utilisateur est responsable
    @OneToMany(cascade = CascadeType.ALL, mappedBy="responsable")
    @JsonIgnore
    private Collection<ActiviteRessource> activiteRessources;

   /* @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pk.responsable", fetch = FetchType.LAZY)
    private Collection<ActiviteRessource> activiteRessources;*/

}
