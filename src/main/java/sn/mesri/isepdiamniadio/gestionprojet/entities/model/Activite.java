package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import sn.mesri.isepdiamniadio.gestionprojet.entities.enums.Statut;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Data
@Entity
public class Activite implements Serializable{
    @Id
    @GeneratedValue
    private Long id;
    @NotBlank(message = "le libellé ne peut pas être vide")
    @Length(max = 255, message = "libellé très long")
    private String libelle;
    @Column(columnDefinition = "text")
    private String description;
    private int tauxExecutionPhysique;
    private int priorite;
    private Date dateCreation;
    private Date dateModification;
    private Date dateExecutionPrevisionnelle;
    private Date dateExecutionReelle;
    private Date dateFinPrevisionnelle;
    private Date dateFinReelle;
    private Date debutAuPlustart;
    private int dureeExecutionPrevisionnelle;
    private int dureeExecutionReelle;
    @NotNull(message = "le budget ne peut pas être vide")
    private Double budget;
    @Column(columnDefinition = "boolean Default false")
    private Boolean estJalon;
    @Length(max = 255, message = "statut très long")
    private String statutActivite;
    @ManyToOne
    @JoinColumn(name = "activite_parent_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Activite activiteParent;
    // une activité peut avoir plusieurs sous-activités
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "activiteParent",fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<Activite> activites;
    @OrderBy("dateSuivi DESC")
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "activite", fetch = FetchType.LAZY)
    private Collection<SuiviActivite> suiviActivites;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "activite", fetch = FetchType.LAZY)
    private Collection<ValidationActivite> validationActivites;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "activite", fetch = FetchType.LAZY)
    private Collection<Contrainte> contraintes;
    @ManyToOne
    @JoinColumn(name = "lot_id")
    private Lot lot;
    // liste des activités predecesseurs de celle-ci
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pk.predecesseur", fetch = FetchType.LAZY)
    private Collection<LienTache> predecesseurs;
    // liste des activités successeurs de celle-ci
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pk.successeur", fetch = FetchType.LAZY)
    private Collection<LienTache> successeurs;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "activite",fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<Decaissement> decaissements;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pk.activite", fetch = FetchType.LAZY)
    private Collection<ActiviteRessource> activiteRessources;

}
