package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import sn.mesri.isepdiamniadio.gestionprojet.utils.CustomCheminSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
public class Document implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    private Date dateCreation;
    private Date dateModification;
    @NotBlank(message = "le nom ne peut pas être vide")
    @Length(max = 255, message = "le nom est très long")
    private String nom;
    @NotBlank(message = "le chemin ne peut pas être vide")
    @Length(max = 255, message = "chemin très long")
    @JsonSerialize(using = CustomCheminSerializer.class)
    private String chemin;
    @NotBlank(message = "l'extension ne peut pas être vide")
    @Length(max = 255, message = "extension très long")
    private String extension;
    @NotBlank(message = "type de document ne peut pas être vide")
    @Length(max = 255, message = "type de document très long")
    private String typeDocument;
    @Column(columnDefinition = "text")
    private String description;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "suivi_id")
    private SuiviActivite suivi;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "decaissement_id")
    private Decaissement decaissement;
    @ManyToOne
    @JoinColumn(name = "validation_id")
    private Validation validation;

}
