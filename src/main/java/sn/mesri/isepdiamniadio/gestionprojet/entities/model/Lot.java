package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Data
@Entity
public class Lot implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    private Date dateCreation;
    private Date dateModification;
    @NotBlank(message = "le libelle ne peut pas être vide")
    @Length(max = 255, message = "libellé très long")
    private String libelle;
    @Column(columnDefinition = "text")
    private String description;
    private int priorite;
    private Date dateExecutionPrevisionnelle;
    private Date dateExecutionReelle;
    private Date dateFinReelle;
    private Date dateFinPrevisionnelle;
    private int dureeExecutionPrevisionnelle;
    private int dureeExecutionReelle;
    private Double budget;

    @ManyToOne
    @JoinColumn(name = "projet_id")
    private Projet projet;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lot", fetch = FetchType.LAZY)
    private Collection<ValidationLot> validationLots;
    @JsonIgnore
    @OrderBy("dateExecutionPrevisionnelle ASC")
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lot", fetch = FetchType.LAZY)
    private Collection<Activite> activites;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lot", fetch = FetchType.LAZY)
    private Collection<Contrainte> contraintes;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pk.lot", fetch = FetchType.LAZY)
    private Collection<LotOrganisation> lotOrganisations;

}
