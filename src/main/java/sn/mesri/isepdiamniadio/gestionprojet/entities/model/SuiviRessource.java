package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@Entity
public class SuiviRessource extends Suivi{
    @ManyToOne
    @JoinColumn(name = "ressource_id")
    private Ressource ressource;
    private Double quantite;
}
