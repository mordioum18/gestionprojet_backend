package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
public class Financement implements Serializable{
    @Id
    @GeneratedValue
    private Long id;
    private Date dateCreation;
       private Date dateModification;
    @NotBlank(message = "le libelle ne peut pas être vide")
    @Length(max = 255, message = "libellé très long")
    private String libelle;
    @Column(columnDefinition = "text")
    private String description;
    private Double montant;
    private Date dateFinancement;
    @ManyToOne
    @JoinColumn(name = "projet_id")
    private Projet projet;

}
