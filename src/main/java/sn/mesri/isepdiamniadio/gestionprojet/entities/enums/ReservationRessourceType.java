package sn.mesri.isepdiamniadio.gestionprojet.entities.enums;

public enum ReservationRessourceType implements EnumType<String> {
    VAL("VAL","Validée"),
    PROP("PROP","Proposée");

    private String id;
    private String name;

    ReservationRessourceType(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName(){
        return name;
    }

    @Override
    public String getId() {
        return id;
    }


    public static Boolean findByName(String name) {
        for(ReservationRessourceType reservationRessourceType : values()) {
            if(reservationRessourceType.name.equals(name))
                return true;
        }
        return  false;
    }

}
