package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Data
@Entity
public class Organisation implements Serializable{
    @Id
    @GeneratedValue
    private Long id;
    private Date dateCreation;
    private Date dateModification;
    @NotBlank(message = "la raison sociale ne peut pas être vide")
    @Length(max = 255, message = "raison sociale très long")
    private String raisonSociale;
    @Length(max = 255, message = "adresse très longue")
    private String adresse;
    @Length(max = 255, message = "téléphone très long")
    private String telephone;
    @Length(max = 255, message = "email très long")
    private String email;
    @Length(max = 255, message = "site web très long")
    private String siteWeb;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "organisation", fetch = FetchType.LAZY)
    private Collection<Utilisateur> utilisateurs;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pk.organisation", fetch = FetchType.LAZY)
    private Collection<LotOrganisation> lotOrganisations;

}
