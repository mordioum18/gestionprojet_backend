package sn.mesri.isepdiamniadio.gestionprojet.entities.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Data
@Entity
public class UtilisateurProjet {
    @EmbeddedId
    private UtilisateurProjetPk pk;
    private String role;

}
