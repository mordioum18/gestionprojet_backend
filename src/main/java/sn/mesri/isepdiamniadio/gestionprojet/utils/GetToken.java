package sn.mesri.isepdiamniadio.gestionprojet.utils;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

public class GetToken {
    public static String getToken(String username, String password){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String auth = "projet-isep-diamniadio:secret";
        byte[] encodedAuth = org.apache.commons.codec.binary.Base64.encodeBase64(
                auth.getBytes(Charset.forName("ISO-8859-1")));
        headers.set("Authorization", "Basic " + new String(encodedAuth));

        HttpEntity<String> entity = new HttpEntity<String>("",headers);
        String authURL = "http://localhost:8080/oauth/token?grant_type=password&username="+username+"&password="+password;
        ResponseEntity<String> response = restTemplate.postForEntity(authURL, entity, String.class);
        //System.out.println("########################################");
        //System.out.println(response.getBody());
        String[] tab= response.getBody().split("\"");
        return tab[3];
    }
}
