package sn.mesri.isepdiamniadio.gestionprojet.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;

public class CustomCheminSerializer extends StdSerializer<String> {
    @Value("${sourceFile}")
    private String sourceFile;
    public CustomCheminSerializer() {
        this(null);
    }

    public CustomCheminSerializer(Class<String> t) {
        super(t);
    }
    @Override
    public void serialize(String s, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(Base64EncodeDecode.encoder(s));
    }
}
