package sn.mesri.isepdiamniadio.gestionprojet.exception;

public class BadRequestException extends RuntimeException{

    public BadRequestException(String message) {
        super(message);
    }

}
