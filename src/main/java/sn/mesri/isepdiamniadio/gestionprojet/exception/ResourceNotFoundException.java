package sn.mesri.isepdiamniadio.gestionprojet.exception;

public class ResourceNotFoundException extends RuntimeException{

    public ResourceNotFoundException(String message) {
        super(message);
    }

}
