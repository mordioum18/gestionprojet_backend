package sn.mesri.isepdiamniadio.gestionprojet.config;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import sn.mesri.isepdiamniadio.gestionprojet.entities.enums.Role;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Utilisateur;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.UtilisateurProjet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Provides a basic implementation of the UserDetails interface
 */
public class CustomUserDetails implements UserDetails {

    private Collection<? extends GrantedAuthority> authorities;
    private Utilisateur utilisateur;

    public CustomUserDetails(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
        this.authorities = translate(utilisateur.getUtilisateurProjets());
    }


    private Collection<? extends GrantedAuthority> translate(Collection<UtilisateurProjet> utilisateurProjets) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (UtilisateurProjet utilisateurProjet : utilisateurProjets) {
            String role=utilisateurProjet.getRole();
            authorities.add(new SimpleGrantedAuthority(role+"_"+utilisateurProjet.getPk().getProjet().getId()));

        }
        return authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return this.utilisateur.getPassword();
    }

    @Override
    public String getUsername() {
        return this.utilisateur.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.utilisateur.getActif() == true;
    }

}
