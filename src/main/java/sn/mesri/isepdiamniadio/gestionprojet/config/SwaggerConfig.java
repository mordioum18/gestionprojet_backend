
package sn.mesri.isepdiamniadio.gestionprojet.config;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static com.google.common.collect.Lists.newArrayList;


/**
 * Created by hp on 17/11/2017.
 */

//@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .forCodeGeneration(true)
                .pathMapping("/")
                .select()
                .apis(RequestHandlerSelectors.basePackage("sn.mesri.isepdiamniadio.gestionprojet"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET,
                        newArrayList(new ResponseMessageBuilder()
                                        .code(500)
                                        .message("500 message")
                                        .responseModel(new ModelRef("Error"))
                                        .build(),
                                new ResponseMessageBuilder()
                                        .code(403)
                                        .message("Forbidden!")
                                        .build()));
    }

    /*
    private Predicate<String> paths(){
        return Predicates.and(PathSelectors.regex("/.*"),
                Predicates.not(PathSelectors.regex("/error")));
    }
    */

    private ApiInfo apiInfo(){
        Contact contact = new Contact("MESRI/ISEP-DIAMNIADIO", "https://www.isepdiamniadio.com", "isep@gmail.com");
        return new ApiInfoBuilder()
                .title("Manage and Monitoring Project API")
                .description("Some Custom Description of Managing Project API")
                .termsOfServiceUrl("Terms of service")
                .license("Licence of API")
                .licenseUrl("API licence URL")
                .contact(contact)
                .version("1.0")
                .build();
    }
}
