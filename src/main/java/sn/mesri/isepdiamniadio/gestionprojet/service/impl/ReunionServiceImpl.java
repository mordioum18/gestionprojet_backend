package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import sn.mesri.isepdiamniadio.gestionprojet.dao.EvenementRepository;
import sn.mesri.isepdiamniadio.gestionprojet.dao.ReunionRepository;
import sn.mesri.isepdiamniadio.gestionprojet.dao.UtilisateurRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;
import sn.mesri.isepdiamniadio.gestionprojet.exception.ResourceNotFoundException;
import sn.mesri.isepdiamniadio.gestionprojet.service.ReunionService;
import sn.mesri.isepdiamniadio.gestionprojet.utils.CopyNotNullPropertiesUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Service
public class ReunionServiceImpl implements ReunionService{
    @Autowired
    private ReunionRepository reunionRepository;

    @Override
    public Page<Reunion> findAll(Pageable pageable) {
        return reunionRepository.findAll(pageable);
    }

    @Override
    public Reunion findById(Long id) {
        Reunion reunion=reunionRepository.findOne(id);
        if (reunion==null){
            throw new ResourceNotFoundException("Enregistrement introuvable");
        }
        return reunion;
    }

    @Override
    public Reunion insert(Reunion reunion) {
        reunion.setDateCreation(new Date());
        return reunionRepository.save(reunion);
    }

    @Override
    public Reunion update(Reunion reunion) {
        Reunion r=reunionRepository.findOne(reunion.getId());
        if (r==null){
            throw new ResourceNotFoundException("Enregistrement introuvable");
        }
        CopyNotNullPropertiesUtil<Reunion> copy = new CopyNotNullPropertiesUtil<Reunion>();
        copy.copyNonNullProperties(r,reunion);
        r.setDateModification(new Date());
        return reunionRepository.saveAndFlush(r);
    }

    @Override
    public void delete(Long id) {
        Reunion r=reunionRepository.findOne(id);
        if (r==null){
            throw new ResourceNotFoundException("Enregistrement introuvable");
        }
        reunionRepository.delete(id);
    }

    @Override
    public Collection<Projet> getProjets(Long id) {
        Reunion reunion=reunionRepository.findOne(id);
        if (reunion == null){
            throw new ResourceNotFoundException("Cette reunion n\'a pas été pas créée!");
        }
        else {
            return reunion.getProjets();
        }
    }

    @Override
    public Collection<Utilisateur> getUtilisateurs(Long id) {
        Reunion reunion=reunionRepository.findOne(id);
        if (reunion == null){
            throw new ResourceNotFoundException("Cette reunion n\'a pas été pas créée!");
        }
        else {
            Collection<Utilisateur> utilisateurs=new ArrayList<Utilisateur>();
            for (UtilisateurEvenement ue:reunion.getUtilisateurEvenements()){
                utilisateurs.add(ue.getPk().getUtilisateur());
            }
            return utilisateurs;
        }
    }
}
