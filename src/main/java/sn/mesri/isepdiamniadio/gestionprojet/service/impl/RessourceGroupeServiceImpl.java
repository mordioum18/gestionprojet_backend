package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.mesri.isepdiamniadio.gestionprojet.dao.ProjetRepository;
import sn.mesri.isepdiamniadio.gestionprojet.dao.RessourceGroupeRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Lot;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Projet;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Ressource;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.RessourceGroupe;
import sn.mesri.isepdiamniadio.gestionprojet.exception.ResourceNotFoundException;
import sn.mesri.isepdiamniadio.gestionprojet.service.RessourceGroupeService;
import sn.mesri.isepdiamniadio.gestionprojet.utils.CopyNotNullPropertiesUtil;

import java.util.Collection;
import java.util.Date;

@Service
@Transactional
public class RessourceGroupeServiceImpl implements RessourceGroupeService {
    @Autowired
    private RessourceGroupeRepository ressourceGroupeRepository;
    @Autowired
    private ProjetRepository projetRepository;

    @Override
    public Page<RessourceGroupe> findAll(Pageable pageable) {
        return ressourceGroupeRepository.findAll(pageable);
    }

    @Override
    public RessourceGroupe findById(Long id, Long idProjet) {
        RessourceGroupe ressourceGroupe = ressourceGroupeRepository.findOne(id);
        if (ressourceGroupe == null || ressourceGroupe.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("ce groupe de ressources n\'existe pas");
        }
        return ressourceGroupe;
    }

    @Override
    public RessourceGroupe insert(RessourceGroupe ressourceGroupe, Long idProjet) {
        Projet projet = projetRepository.findOne(idProjet);
        if (projet == null){
            throw new ResourceNotFoundException("le projet dont vous voulez créez ce groupe de ressources n\'existe pas ");
        } else {
            ressourceGroupe.setDateCreation(new Date());
            ressourceGroupe.setProjet(projet);
            return ressourceGroupeRepository.save(ressourceGroupe);
        }
    }

    @Override
    public RessourceGroupe update(RessourceGroupe ressourceGroupe, Long idProjet) {
        RessourceGroupe ressourceGroupe_edited = ressourceGroupeRepository.findOne(ressourceGroupe.getId());
        if (ressourceGroupe_edited == null || ressourceGroupe_edited.getProjet().getId() != idProjet){
            throw new ResourceNotFoundException("ce groupe de ressources n\'existe pas");
        } else {
            CopyNotNullPropertiesUtil<RessourceGroupe> copy=new CopyNotNullPropertiesUtil<RessourceGroupe>();
            copy.copyNonNullProperties(ressourceGroupe_edited, ressourceGroupe);
            ressourceGroupe_edited.setDateModification(new Date());
            return ressourceGroupeRepository.saveAndFlush(ressourceGroupe_edited);
        }
    }

    @Override
    public void delete(Long id, Long idProjet) {
        RessourceGroupe ressourceGroupe = ressourceGroupeRepository.findOne(id);
        if (ressourceGroupe == null || ressourceGroupe.getProjet().getId() != idProjet){
            throw new ResourceNotFoundException("ce groupe de ressources n\'existe pas");
        } else {
            ressourceGroupeRepository.delete(id);
        }
    }

    @Override
    public Collection<Ressource> getRessources(Long id, Long idProjet) {
        RessourceGroupe ressourceGroupe = ressourceGroupeRepository.findOne(id);
        if (ressourceGroupe == null || ressourceGroupe.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("ce groupe de ressources n\'existe pas");
        }
        else {
            return ressourceGroupe.getRessources();
        }
    }
}
