package sn.mesri.isepdiamniadio.gestionprojet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Ressource;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.RessourceGroupe;

import java.util.Collection;

public interface RessourceGroupeService {
    Page<RessourceGroupe> findAll(Pageable pageable);
    RessourceGroupe findById(Long id, Long idProjet);
    RessourceGroupe insert(RessourceGroupe ressourceGroupe, Long idProjet);
    RessourceGroupe update(RessourceGroupe ressourceGroupe, Long idProjet);
    void delete(Long id, Long idProjet);
    Collection<Ressource> getRessources(Long id, Long idProjet);
}
