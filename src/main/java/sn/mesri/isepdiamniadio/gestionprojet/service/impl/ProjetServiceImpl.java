package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import sn.mesri.isepdiamniadio.gestionprojet.entities.enums.Profil;
import sn.mesri.isepdiamniadio.gestionprojet.entities.enums.Statut;
import sn.mesri.isepdiamniadio.gestionprojet.exception.BadRequestException;
import sn.mesri.isepdiamniadio.gestionprojet.exception.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import sn.mesri.isepdiamniadio.gestionprojet.dao.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.enums.Role;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;
import sn.mesri.isepdiamniadio.gestionprojet.service.EmailService;
import sn.mesri.isepdiamniadio.gestionprojet.service.ProjetService;
import sn.mesri.isepdiamniadio.gestionprojet.service.UtilisateurService;
import sn.mesri.isepdiamniadio.gestionprojet.utils.CopyNotNullPropertiesUtil;
import sn.mesri.isepdiamniadio.gestionprojet.utils.GetToken;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class ProjetServiceImpl implements ProjetService {
    @Autowired
    private ProjetRepository projetRepository;

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private UtilisateurProjetRepository utilisateurProjetRepository;

    @Autowired
    private UtilisateurService utilisateurService;

    @Autowired
    private TypeProjetRepository typeProjetRepository;

    @Autowired
    private EmailService emailService;


    @Override
    public Page<Projet> findAll(Pageable pageable) {
        return projetRepository.findAll(pageable);
    }

    @Override
    public Projet findById(Long idProjet) {
        Projet projet=projetRepository.findOne(idProjet);
        if (projet==null){
            throw new ResourceNotFoundException("projet introuvable");
        }
        return projet;
    }

    @Override
    public List<Projet> findByNom(String nom) {
        List<Projet> projets=projetRepository.findByNom(nom);
        if (projets==null){
            throw new ResourceNotFoundException("projet introuvable");
        }
        return projets;
    }

    @Override
    public Projet insert(Projet projet) {
        if (projet.getTypeProjet() == null){
            throw new BadRequestException("type de projet non renseigné");
        }
        TypeProjet typeProjet=typeProjetRepository.findOne(projet.getTypeProjet().getId());
        if (typeProjet == null){
            throw new BadRequestException("type de projet inexistant");
        }
        projet.setTypeProjet(typeProjet);
        if (projet.getProjetParent() != null){
            Projet parent = projetRepository.findOne(projet.getProjetParent().getId());
            if (parent == null){
                throw new BadRequestException("projet initial inexistant");
            }
            projet.setProjetParent(parent);
        }
        if (projet.getDateDemarragePrevisionnelle()!=null &&
                projet.getDateReceptionPrevisionnelle()!= null &&
                projet.getDateDemarragePrevisionnelle().after(projet.getDateReceptionPrevisionnelle())){
            throw new BadRequestException("dates incorrectes");
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Utilisateur utilisateur=utilisateurRepository.findByUsername(auth.getName());
        projet.setCreateur(utilisateur);
        projet.setDateFinPrevisionnelle(projet.getDateDemarragePrevisionnelle());
        projet.setDateCreation(new Date());
        projet.setStatutProjet(Statut.TODO.getName());
        projet.setCoutTotal(0.0);
        projet = projetRepository.save(projet);
        UtilisateurProjet utilisateurProjet= new UtilisateurProjet();
        UtilisateurProjetPk utilisateurProjetPk=new UtilisateurProjetPk();
        utilisateurProjetPk.setProjet(projet);
        utilisateurProjetPk.setUtilisateur(utilisateur);
        utilisateurProjet.setPk(utilisateurProjetPk);
        utilisateurProjet.setRole(Role.ADMINPROJET.getName());
        utilisateurProjetRepository.save(utilisateurProjet);
        return projet;
    }

    @Override
    public void delete(Long idProjet) {
        Projet projet=projetRepository.findOne(idProjet);
        if (projet==null){
            throw new ResourceNotFoundException("projet introuvable");
        }
        Collection<UtilisateurProjet> utilisateurProjets = new ArrayList<UtilisateurProjet>(projet.getUtilisateurProjets());
        for (UtilisateurProjet up: utilisateurProjets){
            utilisateurProjetRepository.delete(up.getPk());
        }
        projetRepository.delete(projet);
    }

    @Override
    public Projet update(Projet p) {
        Projet projet=projetRepository.findOne(p.getId());
        if (projet==null){
            throw new ResourceNotFoundException("projet introuvable");
        }
        if (projet.getTypeProjet() == null){
            throw new BadRequestException("type de projet non renseigné");
        }
        TypeProjet typeProjet=typeProjetRepository.findOne(projet.getTypeProjet().getId());
        if (typeProjet == null){
            throw new BadRequestException("type de projet inexistant");
        }
        projet.setTypeProjet(typeProjet);
        if (projet.getProjetParent() != null){
            Projet parent = projetRepository.findOne(projet.getProjetParent().getId());
            if (parent == null){
                throw new BadRequestException("projet initial inexistant");
            }
            projet.setProjetParent(parent);
        }
        p.setCoutTotal(projet.getCoutTotal());
        CopyNotNullPropertiesUtil<Projet> copy=new CopyNotNullPropertiesUtil<Projet>();
        copy.copyNonNullProperties(projet,p);
        projet.setDateModification(new Date());
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Utilisateur utilisateur=utilisateurRepository.findByUsername(auth.getName());
        projet.setCreateur(utilisateur);
        if (!Statut.findByName(projet.getStatutProjet())){
            throw new BadRequestException("Statut incorrect");
        }
        if (projet.getDateDemarragePrevisionnelle()!=null &&
                projet.getDateReceptionPrevisionnelle()!= null &&
                projet.getDateDemarragePrevisionnelle().after(projet.getDateReceptionPrevisionnelle())){
            throw new BadRequestException("dates incorrectes");
        }
        return projetRepository.saveAndFlush(projet);
    }

    @Override
    public Collection<UtilisateurProjet> listActeurs(Long idProjet) {
        Projet projet=projetRepository.findOne(idProjet);
        if (projet==null){
            throw new ResourceNotFoundException("projet introuvable");
        }

        return projet.getUtilisateurProjets();
    }

    @Override
    public UtilisateurProjet addUtilisateurRole(Long idProjet, UtilisateurProjet utilisateurProjet) {
        if (utilisateurProjet.getPk() == null ||
                utilisateurProjet.getPk().getUtilisateur() == null ||
                utilisateurProjet.getPk().getProjet() == null ||
                utilisateurProjet.getPk().getProjet().getId() != idProjet){
            throw new BadRequestException("données invalides") ;
        }
        Projet projet=projetRepository.findOne(idProjet);
        if (projet==null){
            throw new ResourceNotFoundException("projet introuvable");
        }
        if (utilisateurProjet.getPk().getUtilisateur().getEmail()==null){
            throw new BadRequestException("données invalides");
        }
        if (!Role.findByName(utilisateurProjet.getRole())){
            throw new BadRequestException("données invalides");
        }
        String email = utilisateurProjet.getPk().getUtilisateur().getEmail();
        Utilisateur utilisateur=utilisateurRepository.findByEmail(email);
        if (utilisateur == null){
            utilisateurProjet.getPk().getUtilisateur().setActif(true);
            utilisateurProjet.getPk().getUtilisateur().setNom("");
            utilisateurProjet.getPk().getUtilisateur().setPrenom("");
            utilisateurProjet.getPk().getUtilisateur().setUsername(email);
            utilisateurProjet.getPk().getUtilisateur().setTelephone("");
            utilisateurProjet.getPk().getUtilisateur().setProfil(Profil.ACTEURPROJET.getName());
            utilisateurProjet.getPk().getUtilisateur().setPasswordPlain(email+email);
            utilisateur = utilisateurService.insert(utilisateurProjet.getPk().getUtilisateur());

            String token = GetToken.getToken(utilisateur.getUsername(),email+email);
            String message = "Bonjour, \nVous avez été ajouté au projet "+projet.getNom()+". Cliquez sur ce lien (http://localhost:4200/register/"+token+") pour vous connecter au projet.";
            emailService.sendSimpleMessage(email, projet.getNom(),message);
            utilisateur.setActif(false);
            utilisateurService.update(utilisateur);
        }
        else {
            if (utilisateur.getActif()==true){
                String message = "Bonjour, \nVous avez été ajouté au projet "+projet.getNom()+". Cliquez sur ce lien (http://localhost:4200/login) pour vous coonecter.";
                emailService.sendSimpleMessage(email, projet.getNom(),message);
            }
            else {
                utilisateur.setActif(true);
                utilisateurService.update(utilisateur);
                String token = GetToken.getToken(utilisateur.getUsername(),email+email);
                utilisateur.setActif(false);
                utilisateurService.update(utilisateur);
                String message = "Bonjour, \nVous avez été ajouté au projet "+projet.getNom()+". Cliquez sur ce lien (http://localhost:4200/register/"+token+") pour vous connecter au projet.";
                emailService.sendSimpleMessage(email, projet.getNom(),message);
            }
        }
        utilisateurProjet.getPk().setUtilisateur(utilisateur);
        utilisateurProjet.getPk().setProjet(projet);
        return utilisateurProjetRepository.save(utilisateurProjet);
    }

    @Override
    public void deleteUtilisateurProjet(Long idProjet, Long idUtilisateur) {
        Projet projet=projetRepository.findOne(idProjet);
        if (projet==null){
            throw new ResourceNotFoundException("projet introuvable");
        }
        Utilisateur utilisateur=utilisateurRepository.findOne(idUtilisateur);
        if (utilisateur==null){
            throw new ResourceNotFoundException("utilisateur introuvable");
        }
        UtilisateurProjetPk utilisateurProjetPk=new UtilisateurProjetPk();
        utilisateurProjetPk.setProjet(projet);
        utilisateurProjetPk.setUtilisateur(utilisateur);
        UtilisateurProjet up=utilisateurProjetRepository.findOne(utilisateurProjetPk);
        if (up==null){
            throw new ResourceNotFoundException("acteur introuvable");
        }
        projet.getUtilisateurProjets().remove(up);
        utilisateur.getUtilisateurProjets().remove(up);
        utilisateurProjetRepository.delete(utilisateurProjetPk);
    }

    @Override
    public Collection<Lot> getLot(Long idProjet) {
        Projet projet=projetRepository.findOne(idProjet);
        if (projet==null){
            throw new ResourceNotFoundException("projet introuvable");
        }
        return projet.getLots();
    }

    @Override
    public Collection<Activite> getActivite(Long idProjet) {
        Projet projet=projetRepository.findOne(idProjet);
        if (projet==null){
            throw new ResourceNotFoundException("projet introuvable");
        }
        Collection<Activite> activites= new ArrayList<Activite>();
        Collection<Lot> lots = projet.getLots();
        for (Lot lot: lots){
            activites.addAll(lot.getActivites());
        }
        return activites;
    }

    @Override
    public Collection<LienTache> getLiens(Long idProjet) {
        Projet projet=projetRepository.findOne(idProjet);
        if (projet==null){
            throw new ResourceNotFoundException("projet introuvable");
        }
        Collection<LienTache> lienTaches = new ArrayList<LienTache>();
        for (Lot lot: projet.getLots()){
            for (Activite activite: lot.getActivites()){
                lienTaches.addAll(activite.getSuccesseurs());
            }
        }

        return  lienTaches;
    }

    @Override
    public Collection<Financement> getFinancements(Long idProjet) {
        Projet projet=projetRepository.findOne(idProjet);
        if (projet==null){
            throw new ResourceNotFoundException("projet introuvable");
        }
        return projet.getFinancements();
    }

    @Override
    public Collection<Ressource> getRessources(Long id) {
        Projet projet=projetRepository.findOne(id);
        if (projet==null){
            throw new ResourceNotFoundException("projet introuvable");
        }
        return projet.getRessources();
    }

    @Override
    public Collection<RessourceGroupe> getRessourceGroupes(Long id) {
        Projet projet=projetRepository.findOne(id);
        if (projet==null){
            throw new ResourceNotFoundException("projet introuvable");
        }
        return projet.getRessourceGroupes();
    }
}
