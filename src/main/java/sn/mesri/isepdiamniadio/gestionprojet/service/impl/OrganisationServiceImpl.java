package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.dao.LotOrganisationRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;
import sn.mesri.isepdiamniadio.gestionprojet.exception.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import sn.mesri.isepdiamniadio.gestionprojet.dao.OrganisationRepository;
import sn.mesri.isepdiamniadio.gestionprojet.service.OrganisationService;
import sn.mesri.isepdiamniadio.gestionprojet.utils.CopyNotNullPropertiesUtil;

import java.util.Collection;
import java.util.Date;

@Service
public class OrganisationServiceImpl implements OrganisationService {
    @Autowired
    private OrganisationRepository organisationRepository;

    @Override
    public Collection<Organisation> findAll() {
        return organisationRepository.findAll();
    }

    @Override
    public Organisation findById(Long id) {
        Organisation organisation=organisationRepository.findOne(id);
        if (organisation==null){
            throw new ResourceNotFoundException("organisation introuvable");
        }
        return organisation;
    }

    @Override
    public Organisation insert(Organisation organisation) {
        organisation.setDateCreation(new Date());
        return organisationRepository.save(organisation);
    }

    @Override
    public Organisation update(Organisation organisation) {
        Organisation r=organisationRepository.findOne(organisation.getId());
        if (r==null){
            throw new ResourceNotFoundException("organisation introuvable");
        }
        CopyNotNullPropertiesUtil<Organisation> copy=new CopyNotNullPropertiesUtil<Organisation>();
        copy.copyNonNullProperties(r,organisation);
        r.setDateModification(new Date());
        return organisationRepository.saveAndFlush(r);
    }

    @Override
    public void delete(Long id) {
        Organisation r=organisationRepository.findOne(id);
        if (r==null){
            throw new ResourceNotFoundException("organisation introuvable");
        }
        organisationRepository.delete(id);

    }

    @Override
    public Collection<Utilisateur> getUtilisateurs(Long id) {
        Organisation organisation=organisationRepository.findOne(id);
        if (organisation == null){
            throw new ResourceNotFoundException("Cette organisation n\'existe pas!");
        }
        else {
            return organisation.getUtilisateurs();
        }
    }
}
