package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import sn.mesri.isepdiamniadio.gestionprojet.entities.enums.ExtensionDocument;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Document;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Utilisateur;
import sn.mesri.isepdiamniadio.gestionprojet.exception.BadRequestException;
import sn.mesri.isepdiamniadio.gestionprojet.exception.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import sn.mesri.isepdiamniadio.gestionprojet.dao.DecaissementRepository;
import sn.mesri.isepdiamniadio.gestionprojet.dao.UtilisateurRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Decaissement;
import sn.mesri.isepdiamniadio.gestionprojet.service.DecaissementService;
import sn.mesri.isepdiamniadio.gestionprojet.service.DocumentService;
import sn.mesri.isepdiamniadio.gestionprojet.utils.CopyNotNullPropertiesUtil;

import java.io.File;
import java.util.Collection;
import java.util.Date;

@Service
public class DecaissementServiceImpl implements DecaissementService {
    @Autowired
    private DecaissementRepository decaissementRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private DocumentService documentService;

    @Override
    public Page<Decaissement> findAll(Pageable pageable) {
        return decaissementRepository.findAll(pageable);
    }

    @Override
    public Decaissement findById(Long idProjet, Long idDecaissement) {
        Decaissement d=decaissementRepository.findOne(idDecaissement);
        if (d==null || d.getActivite().getLot().getProjet().getId() != idProjet){
            throw new ResourceNotFoundException("décaissement introuvable");
        }
        return d;

    }

    @Override
    public Decaissement insert(Long idProjet, Decaissement decaissement) {
        if (decaissement.getActivite() == null ||
                decaissement.getActivite().getLot() == null ||
                decaissement.getActivite().getLot().getProjet() == null){
            throw new BadRequestException("données invalide");
        }
        if (decaissement.getActivite().getLot().getProjet().getId() != idProjet){
            throw new ResourceNotFoundException("projet introuvable");
        }
        if (decaissement.getDocuments()==null || decaissement.getDocuments().size()==0){
            throw new BadRequestException("pièce jointe introuvable");
        }
        Collection<Document> documents = decaissement.getDocuments();
        for (Document document: documents){
            if (!ExtensionDocument.findByName(document.getExtension()) || document.getTypeDocument()==null || document.getTypeDocument().equals("")){
                throw new BadRequestException("documents invalide");
            }
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Utilisateur utilisateur=utilisateurRepository.findByUsername(auth.getName());
        decaissement.setUtilisateur(utilisateur);
        decaissement.setDateCreation(new Date());
        decaissement.setDocuments(null);
        Decaissement d = decaissementRepository.save(decaissement);
        for (Document document: documents){
            document.setDecaissement(d);
            if(this.documentService.insertDocument(document, idProjet)==null){
                this.delete(idProjet, d.getId());
                throw new BadRequestException("pièce jointe invalide");
            }
        }
        d.setDocuments(documents);
        return d;
    }

    @Override
    public Decaissement update(Long idProjet, Decaissement decaissement) {
        if (decaissement.getActivite() == null ||
                decaissement.getActivite().getLot() == null ||
                decaissement.getActivite().getLot().getProjet() == null){
            throw new BadRequestException("données invalide");
        }
        Decaissement d=decaissementRepository.findOne(decaissement.getId());
        if (d==null || d.getActivite().getLot().getProjet().getId() != idProjet){
            throw new ResourceNotFoundException("décaissement introuvable");
        }
        CopyNotNullPropertiesUtil<Decaissement> copy = new CopyNotNullPropertiesUtil<Decaissement>();
        copy.copyNonNullProperties(d,decaissement);
        d.setDateModification(new Date());
        return decaissementRepository.saveAndFlush(d);
    }

    @Override
    public void delete(Long idProjet, Long idDecaissement) {
        Decaissement d=decaissementRepository.findOne(idDecaissement);
        if (d==null || d.getActivite().getLot().getProjet().getId() != idProjet){
            throw new ResourceNotFoundException("décaissement introuvable");
        }
        for (Document doc: d.getDocuments()){
            try{
                File file = new File(doc.getChemin());

                if(file.delete()){
                    System.out.println(file.getName() + " is deleted!");
                }else{
                    System.out.println("Delete operation is failed.");
                }

            }catch(Exception e){
                e.printStackTrace();
            }
        }
        decaissementRepository.delete(idDecaissement);
    }

}
