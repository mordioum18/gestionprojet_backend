package sn.mesri.isepdiamniadio.gestionprojet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Document;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.SuiviRessource;

import java.util.Collection;

public interface SuiviRessourceService {
    Page<SuiviRessource> findAll(Pageable pageable);
    SuiviRessource findById(Long idProjet, Long idSuiviRessource);
    SuiviRessource insert(SuiviRessource suiviRessource, Long idProjet);
    SuiviRessource update(SuiviRessource suiviRessource, Long idProjet);
    void delete(Long idProjet,Long idSuiviRessource);
}
