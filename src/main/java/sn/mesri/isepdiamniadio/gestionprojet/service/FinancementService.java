package sn.mesri.isepdiamniadio.gestionprojet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Financement;

import java.util.Collection;

public interface FinancementService {
    Page<Financement> findAll(Pageable pageable);
    Financement findById(Long idProjet,Long idFinancement);
    Financement insert(Financement financement, Long idProjet);
    Financement update(Financement financement, Long idProjet);
    void delete(Long idProjet,Long idFinancement);
}