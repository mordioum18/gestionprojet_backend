package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.exception.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import sn.mesri.isepdiamniadio.gestionprojet.dao.FinancementRepository;
import sn.mesri.isepdiamniadio.gestionprojet.dao.ProjetRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Financement;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Projet;
import sn.mesri.isepdiamniadio.gestionprojet.service.FinancementService;

import java.util.Collection;
import java.util.Date;

@Service
public class FinancementServiceImpl implements FinancementService {
    @Autowired
    private FinancementRepository financementRepository;
    @Autowired
    private ProjetRepository projetRepository;

    @Override
    public Page<Financement> findAll(Pageable pageable) {
        return financementRepository.findAll(pageable);
    }

    @Override
    public Financement findById(Long idProjet, Long idFinancement) {
        Financement financement=financementRepository.findOne(idFinancement);
        if (financement==null || financement.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("financement introuvable");
        }
        return financement;
    }

    @Override
    public Financement insert(Financement financement, Long idProjet) {
        Projet projet = projetRepository.findOne(idProjet);
        if (projet == null){
            throw new ResourceNotFoundException("le projet dont vous voulez ajoutez un financement, n\'existe pas ");
        } else {
            financement.setDateCreation(new Date());
            financement.setProjet(projet);
            return financementRepository.save(financement);
        }
    }

    @Override
    public Financement update(Financement financement, Long idProjet) {
        Financement r=financementRepository.findOne(financement.getId());
        if (r==null || r.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("financement introuvable");
        }
        financement.setDateModification(new Date());
        return financementRepository.saveAndFlush(financement);
    }

    @Override
    public void delete(Long idProjet, Long idFinancement) {
        Financement r=financementRepository.findOne(idFinancement);
        if (r==null || r.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("financement introuvable");
        }
        financementRepository.delete(idFinancement);

    }
}
