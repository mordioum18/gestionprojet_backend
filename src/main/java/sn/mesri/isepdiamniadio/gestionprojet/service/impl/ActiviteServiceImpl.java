package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.mesri.isepdiamniadio.gestionprojet.dao.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.enums.*;
import sn.mesri.isepdiamniadio.gestionprojet.exception.BadRequestException;
import sn.mesri.isepdiamniadio.gestionprojet.exception.ResourceNotFoundException;
import sn.mesri.isepdiamniadio.gestionprojet.service.ActiviteService;
import sn.mesri.isepdiamniadio.gestionprojet.utils.CopyNotNullPropertiesUtil;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

@Service
@Transactional
public class ActiviteServiceImpl implements ActiviteService {
    @Autowired
    private ActiviteRepository activiteRepository;
    @Autowired
    private LotRepository lotRepository;
    @Autowired
    private ProjetRepository projetRepository;
    @Autowired
    private LienTacheRepository lienTacheRepository;
    @Autowired
    private ActiviteRessourceRepository activiteRessourceRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Override
    public Activite insert(Activite activite,Long idProjet) {
        if (activite.getLot()== null){
            throw new ResourceNotFoundException("choissisez un lot");
        }
        else {
            Lot lot = lotRepository.findOne(activite.getLot().getId());
            if (lot == null || lot.getProjet().getId()!=idProjet){
                throw new ResourceNotFoundException("le lot dont vous voulez ajoutez l\'activité, n\'existe pas ");
            }
        }
        if (activite.getActiviteParent() != null){
            Activite activite_parent = activiteRepository.findOne(activite.getActiviteParent().getId());
            if(activite_parent != null && activite_parent.getLot().getProjet().getId() != idProjet){
                throw new ResourceNotFoundException("l'activité parent n\'existe pas");
            }
            if (activite.getDateExecutionPrevisionnelle()==null || activite.getDateExecutionPrevisionnelle().equals("")){
                activite.setDateExecutionPrevisionnelle(activite.getActiviteParent().getDateExecutionPrevisionnelle());
            }
            else {
                if (activite.getDateExecutionPrevisionnelle().before(activite.getLot().getProjet().getDateDemarragePrevisionnelle())){
                    throw new BadRequestException("date d'exécution prévue incorrecte");
                }
            }
            activite.setDateFinPrevisionnelle(calculDateFin(activite.getDateExecutionPrevisionnelle(), activite.getDureeExecutionPrevisionnelle()));
        }
        else {
            if (activite.getDateExecutionPrevisionnelle()==null || activite.getDateExecutionPrevisionnelle().equals("")){
                activite.setDateExecutionPrevisionnelle(activite.getLot().getDateExecutionPrevisionnelle());
            }
            else {
                if (activite.getDateExecutionPrevisionnelle().before(activite.getLot().getProjet().getDateDemarragePrevisionnelle())){
                    throw new BadRequestException("date d'exécution incorrecte");
                }
            }
            activite.setDateFinPrevisionnelle(calculDateFin(activite.getDateExecutionPrevisionnelle(), activite.getDureeExecutionPrevisionnelle()));
        }
        if (activite.getDateExecutionReelle()==null || activite.getDateExecutionReelle().equals("")){
            activite.setDateExecutionReelle(activite.getDateExecutionPrevisionnelle());
        }
        else {
            if (activite.getDateExecutionReelle().before(activite.getLot().getProjet().getDateDemarragePrevisionnelle())){
                throw new BadRequestException("date d'exécution réelle incorrecte");
            }
        }
        if (activite.getBudget()==null || activite.getBudget().equals("")){
            activite.setBudget(0.0);
        }
        activite.setDateCreation(new Date());
        Activite a = activiteRepository.save(activite);
        Lot lot = lotRepository.findOne(a.getLot().getId());
        if (lot.getActivites().size() == 0){
            lot.setDateExecutionPrevisionnelle(activite.getDateExecutionPrevisionnelle());
            lot.setDateFinPrevisionnelle(activite.getDateFinPrevisionnelle());
        }
        else {
            if (lot.getDateExecutionPrevisionnelle().after(a.getDateExecutionPrevisionnelle())){
                lot.setDateExecutionPrevisionnelle(a.getDateExecutionPrevisionnelle());
            }
            if (lot.getDateFinPrevisionnelle().before(a.getDateFinPrevisionnelle())){
                lot.setDateFinPrevisionnelle(a.getDateFinPrevisionnelle());
            }
        }
        lot.setBudget(lot.getBudget()+a.getBudget());
        lotRepository.saveAndFlush(lot);
        Projet projet = projetRepository.findOne(lot.getProjet().getId());
        if (projet.getDateFinPrevisionnelle().before(lot.getDateFinPrevisionnelle())){
            projet.setDateFinPrevisionnelle(lot.getDateFinPrevisionnelle());
        }
        projet.setCoutTotal(projet.getCoutTotal()+a.getBudget());
        projetRepository.saveAndFlush(projet);
        return activiteRepository.findOne(a.getId());
    }

    @Override
    public Activite findById(Long idProjet,Long idActivite) {
        Activite activite = activiteRepository.findOne(idActivite);

        if (activite == null || activite.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException(" Cette activité n\'existe pas. ");
        }
        else {
            return activite;
        }
    }

    @Override
    public Page<Activite> findAll(Pageable pageable) {
        return activiteRepository.findAll(pageable);
    }

    @Override
    public Activite update(Activite activite, Long idProjet) {
        Activite activite_update = activiteRepository.findOne(activite.getId());
        if (activite_update == null || activite_update.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException(" Cette activité n\'existe pas. ");
        }
        if (activite.getLot()== null){
            throw new ResourceNotFoundException("choissisez un lot");
        }
        else {
            Lot lot = lotRepository.findOne(activite.getLot().getId());
            if (lot == null || lot.getProjet().getId()!=idProjet){
                throw new ResourceNotFoundException("le lot dont vous voulez ajoutez l\'activité, n\'existe pas ");
            }

        }
        if (activite.getDateExecutionPrevisionnelle()==null || activite.getDateExecutionPrevisionnelle().equals("")){
            activite.setDateExecutionPrevisionnelle(activite_update.getDateExecutionPrevisionnelle());
        }
        else {
            if (activite.getDateExecutionPrevisionnelle().before(activite.getLot().getProjet().getDateDemarragePrevisionnelle())){
                throw new BadRequestException("date d'exécution prévue incorrecte");
            }
        }
        if (activite.getDateExecutionReelle()==null || activite.getDateExecutionReelle().equals("")){
            activite.setDateExecutionReelle(activite.getDateExecutionPrevisionnelle());
        }
        else {
            if (activite.getDateExecutionReelle().before(activite.getLot().getProjet().getDateDemarragePrevisionnelle())
                    || activite.getDateExecutionReelle().before(activite.getDateExecutionPrevisionnelle())){
                throw new BadRequestException("date d'exécution réelle incorrecte");
            }
        }
        if (activite_update.getSuccesseurs().size()!=0 && activite_update.getDateExecutionPrevisionnelle().getTime()!=activite.getDateExecutionPrevisionnelle().getTime()){
            throw new BadRequestException("date d'exécution prévue incorrecte");
        }
        activite.setDateFinPrevisionnelle(calculDateFin(activite.getDateExecutionPrevisionnelle(), activite.getDureeExecutionPrevisionnelle()));
        if (activite.getActiviteParent() != null){
            Activite activite_parent = activiteRepository.findOne(activite.getActiviteParent().getId());
            if(activite_parent != null && activite_parent.getLot().getProjet().getId() != idProjet){
                throw new ResourceNotFoundException("l'activité parent n\'existe pas");
            }
            activite.setLot(activite_parent.getLot());

        }
        else {
            activite_update.setActiviteParent(null);
        }
        CopyNotNullPropertiesUtil<Activite> copy= new CopyNotNullPropertiesUtil<Activite>();
        copy.copyNonNullProperties(activite_update,activite);
        if (activite.getBudget()==null || activite.getBudget().equals("")){
            activite_update.setBudget(0.0);
        }
        activite_update.setDateModification(new Date());
        Activite act= activiteRepository.saveAndFlush(activite_update);
        Lot lot = lotRepository.findOne(act.getLot().getId());
        lot.setBudget(0.0);
        lot.setDateExecutionPrevisionnelle(act.getDateExecutionPrevisionnelle());
        lot.setDateFinPrevisionnelle(act.getDateFinPrevisionnelle());
        for (Activite a: lot.getActivites()){
            if (lot.getDateExecutionPrevisionnelle().after(a.getDateExecutionPrevisionnelle())){
                lot.setDateExecutionPrevisionnelle(a.getDateExecutionPrevisionnelle());
            }
            if (lot.getDateFinPrevisionnelle().before(a.getDateFinPrevisionnelle())){
                lot.setDateFinPrevisionnelle(a.getDateFinPrevisionnelle());
            }
            lot.setBudget(lot.getBudget()+a.getBudget());
        }
        lotRepository.saveAndFlush(lot);
        if (act.getPredecesseurs()!=null){
            updateSuccesseurs(act.getPredecesseurs());
        }
        Projet projet = projetRepository.findOne(lot.getProjet().getId());
        projet.setCoutTotal(0.0);
        projet.setDateFinPrevisionnelle(lot.getDateFinPrevisionnelle());
        for (Lot l:projet.getLots()){
            if (l.getDateFinPrevisionnelle().after(projet.getDateFinPrevisionnelle())){
                projet.setDateFinPrevisionnelle(l.getDateFinPrevisionnelle());
            }
            projet.setCoutTotal(projet.getCoutTotal()+l.getBudget());
        }
        projetRepository.saveAndFlush(projet);
        return activiteRepository.findOne(act.getId());
    }

    @Override
    public void delete(Long idProjet, Long idActivite) {
        Activite act = activiteRepository.findOne(idActivite);
        if (act == null || act.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException(" Cette activité n\'existe pas. ");
        }
        Long idLot = act.getLot().getId();
        Lot lot = lotRepository.findOne(idLot);
        lot.getActivites().remove(act);
        if (act.getActivites()!=null){
            for (Activite a: act.getActivites()){
                lot.getActivites().remove(a);
                activiteRepository.delete(a);
            }
        }
        activiteRepository.delete(act);
        if (lot.getActivites().size()==0){
            lot.setBudget(0.0);
            lot.setDateExecutionPrevisionnelle(lot.getProjet().getDateDemarragePrevisionnelle());
            lot.setDateFinPrevisionnelle(lot.getProjet().getDateDemarragePrevisionnelle());
            lot.setDateExecutionReelle(lot.getDateExecutionPrevisionnelle());
        }
        else {
            lot.setBudget(0.0);
            lot.setDateExecutionPrevisionnelle(null);
            lot.setDateFinPrevisionnelle(null);
            lot.setDateExecutionReelle(null);
            for (Activite a: lot.getActivites()){
                if (lot.getDateExecutionPrevisionnelle()==null){
                    lot.setDateExecutionPrevisionnelle(a.getDateExecutionPrevisionnelle());
                }
                if (lot.getDateExecutionReelle()==null){
                    lot.setDateExecutionReelle(a.getDateExecutionReelle());
                }
                if (lot.getDateFinPrevisionnelle()==null){
                    lot.setDateFinPrevisionnelle(a.getDateFinPrevisionnelle());
                }
                if (lot.getDateExecutionPrevisionnelle().after(a.getDateExecutionPrevisionnelle())){
                    lot.setDateExecutionPrevisionnelle(a.getDateExecutionPrevisionnelle());
                }
                if (lot.getDateExecutionReelle().after(a.getDateExecutionReelle())){
                    lot.setDateExecutionReelle(a.getDateExecutionReelle());
                }
                if (lot.getDateFinPrevisionnelle().before(a.getDateFinPrevisionnelle())){
                    lot.setDateFinPrevisionnelle(a.getDateFinPrevisionnelle());
                }
                lot.setBudget(lot.getBudget()+a.getBudget());
            }
        }
        lotRepository.saveAndFlush(lot);
        Projet projet = projetRepository.findOne(lot.getProjet().getId());
        projet.setCoutTotal(0.0);
        projet.setDateFinPrevisionnelle(lot.getDateFinPrevisionnelle());
        for (Lot l:projet.getLots()){
            if (l.getDateFinPrevisionnelle().after(projet.getDateFinPrevisionnelle())){
                projet.setDateFinPrevisionnelle(l.getDateFinPrevisionnelle());
            }
            projet.setCoutTotal(projet.getCoutTotal()+l.getBudget());
        }
        projetRepository.saveAndFlush(projet);
    }

    @Override
    public Collection<SuiviActivite> getSuiviActivites(Long idProjet, Long idActivite) {
        Activite activite =activiteRepository.findOne(idActivite);
        if (activite == null || activite.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("Cette activité n\'existe pas!");
        }
        else {
            return activite.getSuiviActivites();
        }
    }

    @Override
    public Collection<ValidationActivite> getValidationActivites(Long idProjet, Long idActivite) {
        Activite activite=activiteRepository.findOne(idActivite);
        if (activite == null || activite.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("Cette activité n\'existe pas!");
        }
        else {
            return activite.getValidationActivites();
        }
    }

    @Override
    public Collection<Contrainte> getContraintes(Long idProjet, Long idActivite) {
        Activite activite=activiteRepository.findOne(idActivite);
        if (activite == null || activite.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("Cette activité n\'existe pas!");
        }
        else {
            return activite.getContraintes();
        }
    }

    @Override
    public Collection<Activite> getActivitesFilles(Long idProjet, Long idActivite) {
        Activite activite=activiteRepository.findOne(idActivite);
        if (activite == null || activite.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("Cette activité n\'existe pas!");
        }
        else {
            return activite.getActivites();
        }
    }

    @Override
    public LienTache addLink(Long idProjet, Long idPredecesseur, Long idSucceseur, String type) {
        Activite predecesseur=activiteRepository.findOne(idPredecesseur);
        if (predecesseur == null || predecesseur.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("Cette activité n\'existe pas!");
        }
        Activite successeur=activiteRepository.findOne(idSucceseur);
        if (successeur == null || successeur.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("Cette activité n\'existe pas!");
        }
        LienTache lienTache = new LienTache();
        LienTachePk lienTachePk = new LienTachePk();
        lienTachePk.setPredecesseur(predecesseur);
        lienTachePk.setSuccesseur(successeur);
        lienTache.setPk(lienTachePk);
        lienTache.setCodeType(type);
        if (type.equals("0")){
            lienTache.setLibelleType(LinkTacheType.FINDEBUT.getName());
        }
        else {
            throw new BadRequestException("Lien incorrect");
        }
        LienTache l = lienTacheRepository.save(lienTache);
        if (l != null){
            updateSuccesseur(predecesseur, successeur);
            Projet projet = projetRepository.findOne(predecesseur.getLot().getProjet().getId());
            projet.setDateFinPrevisionnelle(predecesseur.getLot().getDateFinPrevisionnelle());
            for (Lot lot: projet.getLots()){
                if (lot.getDateFinPrevisionnelle().after(projet.getDateFinPrevisionnelle())){
                    projet.setDateFinPrevisionnelle(lot.getDateFinPrevisionnelle());
                }
            }
            projetRepository.saveAndFlush(projet);
        }
        return l;
    }

    @Override
    public void deleteLink(Long idProjet, Long idPredecesseur, Long idSucceseur) {
        Activite predecesseur=activiteRepository.findOne(idPredecesseur);
        if (predecesseur == null || predecesseur.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("Cette activité n\'existe pas!");
        }
        Activite successeur=activiteRepository.findOne(idSucceseur);
        if (successeur == null || successeur.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("Cette activité n\'existe pas!");
        }
        LienTachePk lienTachePk= new LienTachePk();
        lienTachePk.setPredecesseur(predecesseur);
        lienTachePk.setSuccesseur(successeur);
        LienTache lienTache =lienTacheRepository.findOne(lienTachePk);
        if (lienTache == null){
            throw new ResourceNotFoundException("le lien n\'existe pas!");
        }
        successeur.getSuccesseurs().remove(lienTache);
        lienTacheRepository.delete(lienTache);
        Date maxDateFin = null;
        for (LienTache l: successeur.getSuccesseurs()){
            if (maxDateFin == null){
                maxDateFin = l.getPk().getPredecesseur().getDateFinPrevisionnelle();
            }
            if (maxDateFin.before(l.getPk().getPredecesseur().getDateFinPrevisionnelle())){
                maxDateFin = l.getPk().getPredecesseur().getDateFinPrevisionnelle();
            }
        }
        if (maxDateFin == null){
            if (successeur.getActiviteParent()!=null){
                maxDateFin = successeur.getActiviteParent().getDateExecutionPrevisionnelle();
            }
            else {
                maxDateFin = successeur.getLot().getDateExecutionPrevisionnelle();
            }
        }
        successeur.setDateExecutionPrevisionnelle(maxDateFin);
        successeur.setDateFinPrevisionnelle(calculDateFin(maxDateFin, successeur.getDureeExecutionPrevisionnelle()));
        activiteRepository.saveAndFlush(successeur);
        Lot lot = lotRepository.findOne(successeur.getLot().getId());
        lot.setDateExecutionPrevisionnelle(successeur.getDateExecutionPrevisionnelle());
        lot.setDateFinPrevisionnelle(successeur.getDateFinPrevisionnelle());
        lot.setDateExecutionReelle(successeur.getDateExecutionReelle());
        for (Activite activite: lot.getActivites()){
            if (activite.getDateExecutionPrevisionnelle().before(lot.getDateExecutionPrevisionnelle())){
                lot.setDateExecutionPrevisionnelle(activite.getDateExecutionPrevisionnelle());
            }
            if (activite.getDateFinPrevisionnelle().after(lot.getDateFinPrevisionnelle())){
                lot.setDateFinPrevisionnelle(activite.getDateFinPrevisionnelle());
            }
            if (activite.getDateExecutionReelle().before(lot.getDateExecutionReelle())){
                lot.setDateExecutionReelle(activite.getDateExecutionReelle());
            }
        }
        lotRepository.saveAndFlush(lot);
        if (successeur.getPredecesseurs()!=null){
            updateSuccesseurs(successeur.getPredecesseurs());
        }
        Projet projet = projetRepository.findOne(successeur.getLot().getProjet().getId());
        projet.setDateFinPrevisionnelle(predecesseur.getLot().getDateFinPrevisionnelle());
        for (Lot l: projet.getLots()){
            if (l.getDateFinPrevisionnelle().after(projet.getDateFinPrevisionnelle())){
                projet.setDateFinPrevisionnelle(l.getDateFinPrevisionnelle());
            }
        }
        projetRepository.saveAndFlush(projet);

    }

    @Override
    public Collection<Decaissement> getDecaissements(Long idProjet, Long idActivite) {
        Activite activite=activiteRepository.findOne(idActivite);
        if (activite == null || activite.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("Cette activité n\'existe pas!");
        }
        return activite.getDecaissements();
    }

    @Override
    public Collection<Activite> getSuccesseurs(Long idProjet, Long idActivite) {
        Activite activite=activiteRepository.findOne(idActivite);
        if (activite == null || activite.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("Cette activité n\'existe pas!");
        }
        Collection<Activite> activites = new ArrayList<Activite>();
        for (LienTache lienTache: activite.getPredecesseurs()){
            activites.add(lienTache.getPk().getSuccesseur());
        }
        return  activites;
    }

    private Date calculDateFin(Date date, int duree){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, duree);
        return cal.getTime();
    }

    private void updateSuccesseur(Activite predecesseur, Activite successeur){
        Date maxDateFin = predecesseur.getDateFinPrevisionnelle();
        for (LienTache lienTache: successeur.getSuccesseurs()){
            if (maxDateFin.before(lienTache.getPk().getPredecesseur().getDateFinPrevisionnelle())){
                maxDateFin = lienTache.getPk().getPredecesseur().getDateFinPrevisionnelle();
            }
        }
        successeur.setDateExecutionPrevisionnelle(maxDateFin);
        successeur.setDateExecutionReelle(maxDateFin);
        successeur.setDateFinPrevisionnelle(calculDateFin(maxDateFin, successeur.getDureeExecutionPrevisionnelle()));
        successeur=activiteRepository.saveAndFlush(successeur);
        activiteRepository.flush();
        Lot lot = lotRepository.findOne(successeur.getLot().getId());
        lot.setDateExecutionPrevisionnelle(successeur.getDateExecutionPrevisionnelle());
        lot.setDateFinPrevisionnelle(successeur.getDateFinPrevisionnelle());
        lot.setDateExecutionReelle(successeur.getDateExecutionReelle());
        for (Activite activite: lot.getActivites()){
            if (activite.getDateExecutionPrevisionnelle().before(lot.getDateExecutionPrevisionnelle())){
                lot.setDateExecutionPrevisionnelle(activite.getDateExecutionPrevisionnelle());
            }
            if (activite.getDateFinPrevisionnelle().after(lot.getDateFinPrevisionnelle())){
                lot.setDateFinPrevisionnelle(activite.getDateFinPrevisionnelle());
            }
            if (activite.getDateExecutionReelle().before(lot.getDateExecutionReelle())){
                lot.setDateExecutionReelle(activite.getDateExecutionReelle());
            }
        }
        lotRepository.saveAndFlush(lot);
        lotRepository.flush();
        if (successeur.getPredecesseurs()!=null){
            updateSuccesseurs(successeur.getPredecesseurs());
        }
    }
    private void updateSuccesseurs(Collection<LienTache> liens){
        Collection<LienTache>lienTaches = new ArrayList<LienTache>(liens);
        for (LienTache lienTache: lienTaches){
            Activite precesseur = activiteRepository.findOne(lienTache.getPk().getPredecesseur().getId());
            Activite successeur = activiteRepository.findOne(lienTache.getPk().getSuccesseur().getId());
            updateSuccesseur(precesseur, successeur);
        }
    }

    @Override
    public ActiviteRessource assignRessourceToActivite(Activite activite, ActiviteRessource ressource, Long idProjet) {
        if(activite == null || activite.getLot().getProjet().getId()!=idProjet) {
            throw new RuntimeException("Cette activité n\'existe pas!");
        } else {
            if(ressource.getPk().getRessource() == null || ressource.getPk().getRessource().getProjet().getId()!=idProjet) {
                throw new RuntimeException("Cette ressource n\'existe pas!");
            } else {
                ressource.setDateAssignation(new Date());
                Authentication auth = SecurityContextHolder.getContext().getAuthentication();
                Utilisateur utilisateur=utilisateurRepository.findByUsername(auth.getName());
                ressource.setResponsable(utilisateur);
                if (ressource.getQuantite() > ressource.getPk().getRessource().getQuantitePrevue()){
                    throw new RuntimeException("Cette quantité est supérieure à celle prévue, renseigner un nombre plus petit!");
                } else {
                    return activiteRessourceRepository.save(ressource);
                }
            }
        }
    }

    @Override
    public ActiviteRessource updateAssignRessourceToActivite(Activite activite, ActiviteRessource ressource, Long idProjet) {
        ActiviteRessource activiteRessource = new ActiviteRessource();
        if(activite == null || activite.getLot().getProjet().getId()!=idProjet) {
            throw new RuntimeException("Cette activité n\'existe pas!");
        } else {
            if(ressource.getPk().getRessource() == null || ressource.getPk().getRessource().getProjet().getId()!=idProjet) {
                throw new RuntimeException("Cette ressource n\'existe pas!");
            } else {
                CopyNotNullPropertiesUtil<ActiviteRessource> copy=new CopyNotNullPropertiesUtil<ActiviteRessource>();
                copy.copyNonNullProperties(activiteRessource,ressource);
                activiteRessource.setDateModification(new Date());
                Authentication auth = SecurityContextHolder.getContext().getAuthentication();
                Utilisateur utilisateur=utilisateurRepository.findByUsername(auth.getName());
                activiteRessource.setResponsable(utilisateur);
                return activiteRessourceRepository.saveAndFlush(ressource);
            }
        }
    }

    @Override
    public Collection<ActiviteRessource> getActiviteRessources(Long id, Long idProjet) {
        Activite activite = activiteRepository.findOne(id);
        if (activite == null || activite.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("Cette activité n\'existe pas!");
        }
        else {
            return activite.getActiviteRessources();
        }
    }

    @Override
    public Collection<Ressource> getRessources(Long id, Long idProjet) {
        Activite activite = activiteRepository.findOne(id);
        if (activite == null){
            throw new ResourceNotFoundException("Cette activité n\'existe pas!");
        }
        else {
            if (activite.getLot().getProjet().getId()!=idProjet){
                throw new ResourceNotFoundException("Cette activité ne concerne pas ce projet.");
            } else {
                Collection<Ressource> ressources = new ArrayList<Ressource>();
                for (ActiviteRessource activiteRessource: activite.getActiviteRessources()) {
                    ressources.add(activiteRessource.getPk().getRessource());
                }
                return ressources;
            }
        }
    }

}
