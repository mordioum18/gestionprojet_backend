package sn.mesri.isepdiamniadio.gestionprojet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;

import java.util.Collection;
import java.util.List;


public interface ProjetService {
    Page<Projet> findAll(Pageable pageable);
    Projet findById(Long idProjet);
    List<Projet> findByNom(String nom);
    Projet insert(Projet projet);
    void delete(Long idProjet);
    Projet update(Projet p);

    Collection<UtilisateurProjet> listActeurs(Long idProjet);
    UtilisateurProjet addUtilisateurRole(Long idProjet, UtilisateurProjet utilisateurProjet);
    void deleteUtilisateurProjet(Long idProjet, Long idUtilisateur);

    Collection<Lot> getLot( Long idProjet);
    Collection<Activite> getActivite(Long idProjet);
    Collection<LienTache> getLiens(Long idProjet);
    Collection<Financement> getFinancements(Long idProjet);
    Collection<Ressource> getRessources(Long id);
    Collection<RessourceGroupe> getRessourceGroupes(Long id);


}
