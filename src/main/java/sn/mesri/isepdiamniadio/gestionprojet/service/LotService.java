package sn.mesri.isepdiamniadio.gestionprojet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;

import java.util.Collection;

public interface LotService {
    Page<Lot> findAll(Pageable pageable);
    Lot findById(Long idProjet,Long idLot);
    Lot insert(Lot lot, Long idProjet);
    Lot update(Lot lot,Long idProjet);
    void delete(Long idProjet,Long idLot);
    Collection<ValidationLot> getValidationLots(Long idProjet,Long idLot);
    Collection<Activite> getActivites(Long idProjet, Long idLot);
    Collection<Contrainte> getContraintes(Long idProjet, Long idLot);
    LotOrganisation assignOrganisationToLot(Lot lot, LotOrganisation organisation, Long idProjet);
    Collection<LotOrganisation> getLotOrganisations(Long id, Long idProjet);
    Collection<Organisation> getOrganisations(Long id, Long idProjet);
}
