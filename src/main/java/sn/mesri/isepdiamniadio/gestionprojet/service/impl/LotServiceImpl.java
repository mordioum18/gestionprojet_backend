package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import sn.mesri.isepdiamniadio.gestionprojet.dao.LotOrganisationRepository;
import sn.mesri.isepdiamniadio.gestionprojet.dao.LotRepository;
import sn.mesri.isepdiamniadio.gestionprojet.dao.ProjetRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;
import sn.mesri.isepdiamniadio.gestionprojet.exception.ResourceNotFoundException;
import sn.mesri.isepdiamniadio.gestionprojet.service.LotService;
import sn.mesri.isepdiamniadio.gestionprojet.utils.CopyNotNullPropertiesUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Service
public class LotServiceImpl implements LotService{
    @Autowired
    private LotRepository lotRepository;
    @Autowired
    private ProjetRepository projetRepository;
    @Autowired
    private LotOrganisationRepository lotOrganisationRepository;

    @Override
    public Page<Lot> findAll(Pageable pageable) {
        return lotRepository.findAll(pageable);
    }

    @Override
    public Lot findById(Long idProjet, Long id) {
        Lot lot=lotRepository.findOne(id);
        if (lot==null || lot.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("ce lot n\'existe pas");
        }
        return lot;
    }

    @Override
    public Lot insert(Lot lot, Long idProjet) {
        Projet projet = projetRepository.findOne(idProjet);
        if (projet == null){
            throw new ResourceNotFoundException("le projet dont vous voulez créez le lot n\'existe pas ");
        } else {
            lot.setDateExecutionPrevisionnelle(projet.getDateDemarragePrevisionnelle());
            lot.setDateFinPrevisionnelle(lot.getDateExecutionPrevisionnelle());
            lot.setDateCreation(new Date());
            lot.setProjet(projet);
            lot.setBudget(0.0);
            return lotRepository.save(lot);
        }
    }

    @Override
    public Lot update(Lot lot,Long idProjet) {
        Lot l=lotRepository.findOne(lot.getId());
        if (l==null || l.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("ce lot n\'existe pas");
        } else {
            CopyNotNullPropertiesUtil<Lot> copy=new CopyNotNullPropertiesUtil<Lot>();
            copy.copyNonNullProperties(l,lot);
            l.setDateModification(new Date());
            return lotRepository.saveAndFlush(l);
        }
    }

    @Override
    public void delete(Long idProjet, Long idLot) {
        Lot l=lotRepository.findOne(idLot);
        if (l==null || l.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("ce lot n\'existe pas");
        } else {
            lotRepository.delete(idLot);
        }
    }

    @Override
    public Collection<ValidationLot> getValidationLots(Long idProjet, Long idLot) {
        Lot lot=lotRepository.findOne(idLot);
        if (lot == null || lot.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("ce lot n\'existe pas");
        }
        else {
            return lot.getValidationLots();
        }
    }

    @Override
    public Collection<Activite> getActivites(Long idProjet,Long idLot) {
        Lot lot=lotRepository.findOne(idLot);
        if (lot == null || lot.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("ce lot n\'existe pas");
        }
        else {
            return lot.getActivites();
        }
    }

    @Override
    public Collection<Contrainte> getContraintes(Long idProjet, Long idLot) {
        Lot lot=lotRepository.findOne(idLot);
        if (lot == null || lot.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("ce lot n\'existe pas");
        }
        else {
            return lot.getContraintes();
        }
    }

    @Override
    public LotOrganisation assignOrganisationToLot(Lot lot, LotOrganisation organisation, Long idProjet) {
        if(lot == null || lot.getProjet().getId()!=idProjet) {
            throw new RuntimeException("Ce lot n\'existe pas!");
        } else {
            if(organisation.getPk().getOrganisation() == null) {
                throw new RuntimeException("Cette organisation n\'existe pas!");
            } else {
                return lotOrganisationRepository.save(organisation);
            }
        }

    }

    @Override
    public Collection<LotOrganisation> getLotOrganisations(Long id, Long idProjet) {
        Lot lot = lotRepository.findOne(id);
        if (lot == null || lot.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("Cette organisation n\'existe pas!");
        }
        else {
            return lot.getLotOrganisations();
        }
    }

    @Override
    public Collection<Organisation> getOrganisations(Long id, Long idProjet) {
        Lot lot=lotRepository.findOne(id);
        if (lot == null){
            throw new ResourceNotFoundException("ce lot nexiste pas");
        }
        else {
            if (lot.getProjet().getId()!=idProjet) {
                throw new ResourceNotFoundException("Ce lot ne concerne pas ce projet.");
            } else {
                Collection<Organisation> organisations = new ArrayList<Organisation>();
                for (LotOrganisation lotOrganisation: lot.getLotOrganisations()) {
                    organisations.add(lotOrganisation.getPk().getOrganisation());
                }
                return organisations;
            }
        }
    }

}
