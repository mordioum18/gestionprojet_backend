package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.exception.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import sn.mesri.isepdiamniadio.gestionprojet.dao.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;
import sn.mesri.isepdiamniadio.gestionprojet.service.ValidationLotService;
import sn.mesri.isepdiamniadio.gestionprojet.utils.CopyNotNullPropertiesUtil;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class ValidationLotServiceImpl implements ValidationLotService {
    @Autowired
    private ValidationLotRepository validationLotRepository;
    @Autowired
    private LotRepository lotRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private ReunionRepository reunionRepository;
    @Autowired
    private VisiteRepository visiteRepository;


    @Override
    public Page<ValidationLot> findAll(Pageable pageable) {
        return validationLotRepository.findAll(pageable);
    }

    @Override
    public ValidationLot findById(Long idProjet,Long idValidationLot) {
        ValidationLot validationLot = validationLotRepository.findOne(idValidationLot);
        if (validationLot == null || validationLot.getLot().getProjet().getId()!=idProjet) {
            throw new ResourceNotFoundException("Validation lot introuvable!");
        }
        return validationLot;
    }

    @Override
    public ValidationLot insert(ValidationLot validationLot,
                                Long idProjet,
                                Long idLot,
                                Long idReunion,
                                Long idVisite) {
        Lot lot = lotRepository.findOne(idLot);
        Reunion reunion = reunionRepository.findOne(idReunion);
        Visite visite = visiteRepository.findOne(idVisite);
        if (lot == null || lot.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("le lot dont vous voulez ajoutez une validation, n\'existe pas ");
        }
        Boolean ok = false;
        if (reunion != null) {
            for (Projet p : reunion.getProjets()) {
                if (p.getId() == idProjet) {
                    ok = true;
                    break;
                }
            }
            if (!ok) {
                throw new ResourceNotFoundException("réunion introuvable");
            }
        }

        if (visite != null) {
            for (Projet p : visite.getProjets()) {
                if (p.getId() == idProjet) {
                    ok = true;
                }
            }
            if (!ok) {
                throw new ResourceNotFoundException("visite introuvable");
            }
        }
        validationLot.setEvenement(reunion);
        validationLot.setEvenement(visite);
        validationLot.setLot(lot);
        validationLot.setDateCreation(new Date());
        return validationLotRepository.save(validationLot);
    }

    @Override
    public void delete(Long idProjet,Long idValidationLot) {
        ValidationLot validationLot = validationLotRepository.findOne(idValidationLot);
        if (validationLot == null || validationLot.getLot().getProjet().getId()!=idProjet) {
            throw new ResourceNotFoundException("Validation lot introuvable!");
        }
        validationLotRepository.delete(idValidationLot);
    }

    @Override
    public ValidationLot update(ValidationLot vl, Long idProjet) {
        ValidationLot validationLot = validationLotRepository.findOne(vl.getId());
        if (validationLot == null || validationLot.getLot().getProjet().getId()!=idProjet) {
            throw new ResourceNotFoundException("Validation lot introuvable!");
        }
        CopyNotNullPropertiesUtil<ValidationLot> copy=new CopyNotNullPropertiesUtil<ValidationLot>();
        copy.copyNonNullProperties(validationLot,vl);
        validationLot.setDateModification(new Date());
        return validationLotRepository.saveAndFlush(validationLot);
    }

    @Override
    public Collection<Document> getDocuments(Long idProjet,Long idValidationLot) {
        ValidationLot validationLot = validationLotRepository.findOne(idValidationLot);
        if (validationLot == null || validationLot.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("Cette validation de lot n\'a pas été pas créée!");
        }
        else {
            return validationLot.getDocuments();
        }
    }


}