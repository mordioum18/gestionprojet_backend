package sn.mesri.isepdiamniadio.gestionprojet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;

import java.util.Collection;

public interface UtilisateurService {
    Utilisateur insert(Utilisateur utilisateur);
    Page<Utilisateur> findAll(Pageable pageable);
    Utilisateur findById(Long id);
    void delete(Long id);
    Utilisateur update(Utilisateur p);
    Collection<UtilisateurProjet> listProjetParticipe(Long id, Pageable pageable);
    Collection<Projet> listProjetUserOnLine();
    Utilisateur findByUsername(String username);
    Utilisateur findByEmail(String email);
    Utilisateur findByTelephone(String telephone);
    Utilisateur findByToken();
    void sendLien(String email);

}
