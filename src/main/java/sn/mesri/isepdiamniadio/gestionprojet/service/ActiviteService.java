package sn.mesri.isepdiamniadio.gestionprojet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;

import java.util.Collection;

public interface ActiviteService {
    Activite insert(Activite activite,Long idProjet);
    Activite findById(Long idProjet,Long idActivite);
    Page<Activite> findAll(Pageable pageable);
    Activite update(Activite activite, Long idProjet);
    void delete(Long idProjet, Long idActivite);
    Collection<SuiviActivite> getSuiviActivites(Long idProjet,Long idActivite);
    Collection<ValidationActivite> getValidationActivites(Long idProjet, Long idActivite);
    Collection<Contrainte> getContraintes(Long idProjet, Long idActivite);
    Collection<Activite> getActivitesFilles(Long idProjet, Long idACtivite); // liste des activités fils de cette activité dont son id est largument
    Collection<Decaissement> getDecaissements(Long idProjet, Long idActivite);
    Collection<Activite> getSuccesseurs(Long idProjet, Long idActivite);
    LienTache addLink(Long idProjet, Long idPredecesseur, Long idSucceseur, String type);
    void deleteLink(Long idProjet, Long idPredecesseur, Long idSucceseur);
    ActiviteRessource assignRessourceToActivite(Activite activite, ActiviteRessource ressource, Long idProjet);
    ActiviteRessource updateAssignRessourceToActivite(Activite activite, ActiviteRessource ressource, Long idProjet);
    Collection<ActiviteRessource> getActiviteRessources(Long id, Long idProjet);
    Collection<Ressource> getRessources(Long id, Long idProjet);
}
