package sn.mesri.isepdiamniadio.gestionprojet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Contrainte;

import java.util.Collection;

public interface ContrainteService {
    Page<Contrainte> findAll(Pageable pageable);
    Contrainte findById(Long idProjet, Long idContrainte);
    Contrainte insert(Contrainte contrainte, Long idProjet);
    Contrainte update(Contrainte contrainte,Long idProjet);
    void delete(Long idProjet,Long idContrainte);
}
