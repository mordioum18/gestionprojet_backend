package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import sn.mesri.isepdiamniadio.gestionprojet.dao.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;
import sn.mesri.isepdiamniadio.gestionprojet.service.ValidationActiviteService;
import sn.mesri.isepdiamniadio.gestionprojet.exception.ResourceNotFoundException;
import sn.mesri.isepdiamniadio.gestionprojet.utils.CopyNotNullPropertiesUtil;

import java.util.Collection;
import java.util.Date;

@Service
public class ValidationActiviteServiceImpl implements ValidationActiviteService {
    @Autowired
    private ValidationActiviteRepository validationActiviteRepository;
    @Autowired
    private ActiviteRepository activiteRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private ReunionRepository reunionRepository;
    @Autowired
    private VisiteRepository visiteRepository;

    @Override
    public Page<ValidationActivite> findAll(Pageable pageable) {
        return validationActiviteRepository.findAll(pageable);
    }

    @Override
    public ValidationActivite findById(Long idProjet, Long idValidationActivite) throws ResourceNotFoundException {
        ValidationActivite validationActivite=validationActiviteRepository.findOne(idValidationActivite);
        if(validationActivite==null || validationActivite.getActivite().getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("validationActivite introuvable");
        }
        return validationActivite;
    }

    @Override
    public ValidationActivite insert(ValidationActivite validationActivite,
                                     Long idProjet,
                                     Long idActivite,
                                     Long idReunion,
                                     Long idVisite) {
        Activite activite = activiteRepository.findOne(idActivite);
        Reunion reunion = reunionRepository.findOne(idReunion);
        Visite visite = visiteRepository.findOne(idVisite);
        if (activite == null || activite.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("l\'activité dont vous voulez ajoutez une validation, n\'existe pas ");
        }
        Boolean ok = false;
        if (reunion != null) {
            for (Projet p : reunion.getProjets()) {
                if (p.getId() == idProjet) {
                    ok = true;
                    break;
                }
            }
            if (!ok) {
                throw new ResourceNotFoundException("réunion introuvable");
            }
        }

        if (visite != null) {
            for (Projet p : visite.getProjets()) {
                if (p.getId() == idProjet) {
                    ok = true;
                }
            }
            if (!ok) {
                throw new ResourceNotFoundException("visite introuvable");
            }
        }
        validationActivite.setActivite(activite);
        validationActivite.setEvenement(reunion);
        validationActivite.setEvenement(visite);
        validationActivite.setDateCreation(new Date());
        return validationActiviteRepository.save(validationActivite);
    }

    @Override
    public ValidationActivite update(ValidationActivite validationActivite, Long idProjet) {
        ValidationActivite vActivite=validationActiviteRepository.findOne(validationActivite.getId());
        if(vActivite==null || vActivite.getActivite().getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("validationActivite introuvable");
        } else {
            CopyNotNullPropertiesUtil<ValidationActivite> copy =new CopyNotNullPropertiesUtil<ValidationActivite>();
            copy.copyNonNullProperties(vActivite,validationActivite);
            vActivite.setDateModification(new Date());
            return validationActiviteRepository.saveAndFlush(vActivite);
        }
    }

    @Override
    public void delete(Long idProjet,Long idValidationActivite){
        ValidationActivite vActivite=validationActiviteRepository.findOne(idValidationActivite);
        if(vActivite==null || vActivite.getActivite().getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("validationActivite introuvable");
        }
        validationActiviteRepository.delete(idValidationActivite);
    }

    @Override
    public Collection<Document> getDocuments(Long idProjet,Long idValidationActivite) {
        ValidationActivite vActivite=validationActiviteRepository.findOne(idValidationActivite);
        if (vActivite == null || vActivite.getActivite().getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("Cette validation d\'activité n\'a pas été pas!");
        }
        else {
            return vActivite.getDocuments();
        }
    }
}
