package sn.mesri.isepdiamniadio.gestionprojet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Decaissement;

import java.util.Collection;

public interface DecaissementService {
    Page<Decaissement> findAll(Pageable pageable);
    Decaissement findById(Long idProjet,Long idDecaissement);
    Decaissement insert(Long idProjet, Decaissement decaissement);
    Decaissement update(Long idProjet, Decaissement decaissement);
    void delete(Long idProjet, Long idDecaissement);
}
