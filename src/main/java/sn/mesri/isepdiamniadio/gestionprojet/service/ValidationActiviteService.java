package sn.mesri.isepdiamniadio.gestionprojet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Document;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.ValidationActivite;

import java.util.Collection;

public interface ValidationActiviteService {
    Page<ValidationActivite> findAll(Pageable pageable);
    ValidationActivite findById(Long idProjet,Long idValidationActivite);
    ValidationActivite insert(ValidationActivite validationActivite,Long idProjet, Long idActivite, Long idReunion, Long idVisite);
    ValidationActivite update(ValidationActivite validationActivite, Long idProjet);
    void delete(Long idProjet, Long idValidationActivite);
    Collection<Document> getDocuments(Long idProjet, Long idValidationActivite);

}
