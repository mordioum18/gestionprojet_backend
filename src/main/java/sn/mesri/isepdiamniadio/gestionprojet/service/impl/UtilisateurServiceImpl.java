package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.exception.BadRequestException;
import sn.mesri.isepdiamniadio.gestionprojet.exception.ResourceNotFoundException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sn.mesri.isepdiamniadio.gestionprojet.dao.UtilisateurRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Projet;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Utilisateur;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.UtilisateurProjet;
import sn.mesri.isepdiamniadio.gestionprojet.service.EmailService;
import sn.mesri.isepdiamniadio.gestionprojet.service.UtilisateurService;
import sn.mesri.isepdiamniadio.gestionprojet.utils.CopyNotNullPropertiesUtil;
import sn.mesri.isepdiamniadio.gestionprojet.utils.GetToken;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Service
public class UtilisateurServiceImpl implements UtilisateurService {
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private EmailService emailService;

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    public Utilisateur insert(Utilisateur utilisateur) {
        Utilisateur user=utilisateurRepository.findByUsername(utilisateur.getUsername());
        if (user!=null){
            throw new IllegalArgumentException("login déjà utilisé");
        }
        user=utilisateurRepository.findByEmail(utilisateur.getEmail());
        if (user!=null){
            throw new IllegalArgumentException("email déjà utilisé");
        }
        user=utilisateurRepository.findByTelephone(utilisateur.getTelephone());
        if (utilisateur.getTelephone()!=null && !utilisateur.getTelephone().equals("") && user!=null){
            throw new IllegalArgumentException("numéro de téléphone déjà utilisé");
        }
        if (utilisateur.getPasswordPlain().length()<4){
            throw new IllegalArgumentException("le mot de passe doit contenir au moins 4 caractères");
        }
        utilisateur.setPassword(getPasswordEncoder().encode(utilisateur.getPasswordPlain()));
        utilisateur.setPasswordPlain("");
        utilisateur.setDateCreation(new Date());
        return utilisateurRepository.save(utilisateur);
    }

    @Override
    public Page<Utilisateur> findAll(Pageable pageable) {
        return utilisateurRepository.findAll(pageable);
    }

    @Override
    public Utilisateur findById(Long id) {
        Utilisateur utilisateur=utilisateurRepository.findOne(id);
        if (utilisateur==null){
            throw new ResourceNotFoundException("utilisateur introuvable");
        }
        return utilisateur;
    }

    @Override
    public void delete(Long id) {
        Utilisateur utilisateur=utilisateurRepository.findOne(id);
        if (utilisateur==null){
            throw new ResourceNotFoundException("utilisateur introuvable");
        }
        utilisateurRepository.delete(id);
    }

    @Override
    public Utilisateur update(Utilisateur p) {
        Utilisateur utilisateur=utilisateurRepository.findOne(p.getId());
        if (utilisateur==null){
            throw new ResourceNotFoundException("utilisateur introuvable");
        }
        Utilisateur user=utilisateurRepository.findByUsername(p.getUsername());
        if (user!=null && user.getId()!=utilisateur.getId()){
            throw new IllegalArgumentException("login déjà utilisé");
        }
        user=utilisateurRepository.findByEmail(p.getEmail());
        if (user!=null && user.getId()!=utilisateur.getId()){
            throw new IllegalArgumentException("email déjà utilisé");
        }
        user=utilisateurRepository.findByTelephone(p.getTelephone());
        if (p.getTelephone()!=null && !p.getTelephone().equals("") && user!=null && user.getId()!=utilisateur.getId()){
            throw new IllegalArgumentException("numéro de téléphone déjà utilisé");
        }
        if (p.getPasswordPlain()!=null && !p.getPasswordPlain().equals("") && p.getPasswordPlain().length()<4){
            throw new IllegalArgumentException("le mot de passe doit contenir au moins 4 caractères");
        }
        if (p.getPasswordPlain()!=null && !p.getPasswordPlain().equals("")){
            utilisateur.setPassword(getPasswordEncoder().encode(p.getPasswordPlain()));
            p.setPasswordPlain("");
        }
        CopyNotNullPropertiesUtil<Utilisateur> copyNotNullPropertiesUtil= new CopyNotNullPropertiesUtil<Utilisateur>();
        copyNotNullPropertiesUtil.copyNonNullProperties(utilisateur,p);
        utilisateur.setDateModification(new Date());
        utilisateur.setPasswordPlain("");
        return utilisateurRepository.saveAndFlush(utilisateur);
    }

    @Override
    public Collection<UtilisateurProjet> listProjetParticipe(Long id, Pageable pageable) {
        Utilisateur utilisateur=utilisateurRepository.findOne(id);
        if (utilisateur==null){
            throw new ResourceNotFoundException("utilisateur introuvable");
        }
        return utilisateur.getUtilisateurProjets();
    }

    @Override
    public Collection<Projet> listProjetUserOnLine() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Utilisateur utilisateur=utilisateurRepository.findByUsername(auth.getName());
        Collection<Projet> projets=new ArrayList<Projet>();
        for (UtilisateurProjet utilisateurProjet:utilisateur.getUtilisateurProjets()){
            projets.add(utilisateurProjet.getPk().getProjet());
        }
        return projets;
    }

    @Override
    public Utilisateur findByUsername(String username) {
        return utilisateurRepository.findByUsername(username);
    }

    @Override
    public Utilisateur findByEmail(String email) {
        return utilisateurRepository.findByEmail(email);
    }

    @Override
    public Utilisateur findByTelephone(String telephone) {
        return utilisateurRepository.findByTelephone(telephone);
    }

    @Override
    public Utilisateur findByToken() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Utilisateur utilisateur=utilisateurRepository.findByUsername(auth.getName());
        return utilisateur;
    }

    @Override
    public void sendLien(String email) {
        Utilisateur utilisateur=utilisateurRepository.findByEmail(email);
        if (utilisateur==null){
            throw new BadRequestException("Adresse email non autorisé");
        }
        utilisateur.setActif(true);
        this.update(utilisateur);
        String token = GetToken.getToken(utilisateur.getUsername(),email+email);
        utilisateur.setActif(false);
        this.update(utilisateur);
        String message = "Bonjour, \nVous avez obtenu un nouveau lien. Cliquez sur ce lien (http://localhost:4200/register/"+token+") pour vous connecter au projet.";
        emailService.sendSimpleMessage(email, "Nouveau lien",message);
    }

}
