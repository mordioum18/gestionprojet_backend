package sn.mesri.isepdiamniadio.gestionprojet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Document;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.ValidationLot;

import java.util.Collection;
import java.util.List;


public interface ValidationLotService {
    Page<ValidationLot> findAll(Pageable pageable);
    ValidationLot findById(Long idProjet, Long idValidationLot);
    ValidationLot insert(ValidationLot validationLot, Long idProjet, Long idLot, Long idReunion, Long idVisite);
    void delete(Long idProjet, Long idValidationLot);
    ValidationLot update(ValidationLot validationLot, Long idProjet);
    Collection<Document> getDocuments(Long idProjet, Long idValidationLot);

}
