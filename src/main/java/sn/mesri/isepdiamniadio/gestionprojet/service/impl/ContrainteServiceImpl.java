package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.exception.BadRequestException;
import sn.mesri.isepdiamniadio.gestionprojet.exception.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.mesri.isepdiamniadio.gestionprojet.dao.ActiviteRepository;
import sn.mesri.isepdiamniadio.gestionprojet.dao.ContrainteRepository;
import sn.mesri.isepdiamniadio.gestionprojet.dao.LotRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Activite;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Contrainte;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Lot;
import sn.mesri.isepdiamniadio.gestionprojet.service.ContrainteService;
import sn.mesri.isepdiamniadio.gestionprojet.utils.CopyNotNullPropertiesUtil;

import java.util.Collection;
import java.util.Date;

@Service
@Transactional
public class ContrainteServiceImpl implements ContrainteService {
    @Autowired
    private ContrainteRepository contrainteRepository;
    @Autowired
    private LotRepository lotRepository;
    @Autowired
    private ActiviteRepository activiteRepository;

    @Override
    public Page<Contrainte> findAll(Pageable pageable) {
        return contrainteRepository.findAll(pageable);
    }

    @Override
    public Contrainte findById(Long idProjet,Long idContrainte) {
        Contrainte c=contrainteRepository.findOne(idContrainte);
        if (c==null){
            throw new ResourceNotFoundException("contrainte introuvable");
        }
        if (c.getLot()==null && c.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("contrainte introuvable");
        }
        if (c.getActivite()==null && c.getActivite().getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("contrainte introuvable");
        }
        return c;
    }

    @Override
    public Contrainte insert(Contrainte contrainte, Long idProjet) {
        if ((contrainte.getLot()==null) && (contrainte.getActivite()==null)){
            throw new ResourceNotFoundException("la contrainte doit forcément concernée une activité ou un lot," +
                    "veuillez renseigner un parmi les deux. ");
        } else { // lot!= null ou bien activite!= null
            if(contrainte.getLot() != null && contrainte.getActivite() != null)
                throw new BadRequestException("Une contrainte est liée soit à un lot ou à une activité");

            if (contrainte.getLot()!=null){
                Lot lot = lotRepository.findOne(contrainte.getLot().getId());
                if(lot == null || lot.getProjet().getId() != idProjet)
                    throw new BadRequestException("Ce lot n'existe pas");
                contrainte.setLot(lot);
            }
            if (contrainte.getActivite() != null ) {
                Activite activite = activiteRepository.findOne(contrainte.getActivite().getId());

                if(activite == null || activite.getLot().getProjet().getId() != idProjet)
                    throw new BadRequestException("Cette activité n'existe pas");
                contrainte.setActivite(activite);
            }
            contrainte.setDateCreation(new Date());
            return contrainteRepository.save(contrainte);
        }
    }

    @Override
    public Contrainte update(Contrainte contrainte, Long idProjet) {
        Contrainte c=contrainteRepository.findOne(contrainte.getId());
        if (c==null){
            throw new ResourceNotFoundException("contrainte introuvable");
        }
        if(contrainte.getLot() != null && contrainte.getActivite() != null)
            throw new BadRequestException("Une contrainte est liée soit à un lot ou à une activité");

        if (c.getLot() !=null && c.getLot().getProjet().getId() != idProjet){
            throw new ResourceNotFoundException("contrainte introuvable");
        }
        if (c.getActivite() != null && c.getActivite().getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("contrainte introuvable");
        }

        CopyNotNullPropertiesUtil<Contrainte> copy=new CopyNotNullPropertiesUtil<Contrainte>();
        copy.copyNonNullProperties(c,contrainte);
        c.setDateModification(new Date());
        return contrainteRepository.saveAndFlush(c);
    }

    @Override
    public void delete(Long idProjet, Long idContrainte) {
        Contrainte c=contrainteRepository.findOne(idContrainte);
        if (c==null){
            throw new ResourceNotFoundException("contrainte introuvable");
        }
        if (c.getLot()!=null && c.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("contrainte introuvable");
        }
        if (c.getActivite()!=null && c.getActivite().getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("contrainte introuvable");
        }

        contrainteRepository.delete(idContrainte);

    }
}
