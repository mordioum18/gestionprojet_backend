package sn.mesri.isepdiamniadio.gestionprojet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Document;

import java.util.Collection;

public interface DocumentService {
    Page<Document> findAll(Pageable pageable);
    Document findDocumentById(Long idProjet,Long idDocument);
    Document insertDocument(Document document, Long idProjet);
    void deleteDocument(Long idPorojet, Long idDocument);
}

