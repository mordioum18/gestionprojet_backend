package sn.mesri.isepdiamniadio.gestionprojet.service;

        import org.springframework.data.domain.Page;
        import org.springframework.data.domain.Pageable;
        import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Document;
        import sn.mesri.isepdiamniadio.gestionprojet.entities.model.SuiviActivite;

        import java.util.Collection;

public interface SuiviActiviteService {
    Page<SuiviActivite> findAll(Pageable pageable);
    SuiviActivite findById(Long idProjet,Long idSuiviActivite);
    SuiviActivite insert(SuiviActivite suiviActivite, Long idProjet);
    void delete(Long idProjet,Long idSuiviActivite);
    Collection<Document> getDocuments(Long idProjet,Long idSuiviActivite);
}
