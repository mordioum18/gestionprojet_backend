package sn.mesri.isepdiamniadio.gestionprojet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Lot;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.LotOrganisation;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Organisation;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Utilisateur;

import java.util.Collection;

public interface OrganisationService {
    Collection<Organisation> findAll();
    Organisation findById(Long id);
    Organisation insert(Organisation organisation);
    Organisation update(Organisation organisation);
    void delete(Long id);
    Collection<Utilisateur> getUtilisateurs(Long id);
}
