package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.mesri.isepdiamniadio.gestionprojet.dao.ProjetRepository;
import sn.mesri.isepdiamniadio.gestionprojet.dao.RessourceRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.enums.Statut;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;
import sn.mesri.isepdiamniadio.gestionprojet.exception.ResourceNotFoundException;
import sn.mesri.isepdiamniadio.gestionprojet.service.RessourceService;
import sn.mesri.isepdiamniadio.gestionprojet.utils.CopyNotNullPropertiesUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Service
@Transactional
public class RessourceServiceImpl implements RessourceService {
    @Autowired
    private RessourceRepository ressourceRepository;
    @Autowired
    private ProjetRepository projetRepository;

    @Override
    public Page<Ressource> findAll(Pageable pageable) {
        return ressourceRepository.findAll(pageable);
    }

    @Override
    public Ressource findById(Long id, Long idProjet) {
        Ressource ressource = ressourceRepository.findOne(id);
        if (ressource == null || ressource.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("cette ressource n\'existe pas");
        }
        return ressource;
    }

    @Override
    public Ressource insert(Ressource ressource, Long idProjet) {
        Projet projet = projetRepository.findOne(idProjet);
        if (projet == null){
            throw new ResourceNotFoundException("le projet dont vous voulez créez cette ressource n\'existe pas ");
        } else {
            ressource.setDateCreation(new Date());
            ressource.setProjet(projet);
            return ressourceRepository.save(ressource);
        }
    }

    @Override
    public Ressource update(Ressource ressource, Long idProjet) {
        Ressource ressource_edited = ressourceRepository.findOne(ressource.getId());
        if (ressource_edited == null || ressource_edited.getProjet().getId() != idProjet){
            throw new ResourceNotFoundException("cette ressource n\'existe pas");
        } else {
            CopyNotNullPropertiesUtil<Ressource> copy=new CopyNotNullPropertiesUtil<Ressource>();
            copy.copyNonNullProperties(ressource_edited, ressource);
            ressource_edited.setDateModification(new Date());
            return ressourceRepository.saveAndFlush(ressource_edited);
        }
    }

    @Override
    public void delete(Long id, Long idProjet) {
        Ressource ressource = ressourceRepository.findOne(id);
        if (ressource == null || ressource.getProjet().getId() != idProjet){
            throw new ResourceNotFoundException("cette ressource n\'existe pas");
        } else {
            ressourceRepository.delete(id);
        }
    }

    @Override
    public Collection<ActiviteRessource> getActiviteRessources(Long id, Long idProjet) {
        Ressource ressource = ressourceRepository.findOne(id);
        if (ressource == null || ressource.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("Cette ressource n\'existe pas!");
        }
        else {
            return ressource.getActiviteRessources();
        }
    }

    @Override
    public Collection<Activite> getActivites(Long id, Long idProjet) {
        Ressource ressource = ressourceRepository.findOne(id);
        if (ressource == null){
            throw new ResourceNotFoundException("Cette ressource n\'existe pas!");
        }
        else {
            if (ressource.getProjet().getId()!=idProjet){
                throw new ResourceNotFoundException("Cette ressource ne concerne pas ce projet.");
            } else {
                Collection<Activite> activites = new ArrayList<Activite>();
                for (ActiviteRessource activiteRessource: ressource.getActiviteRessources()) {
                    activites.add(activiteRessource.getPk().getActivite());
                }
                return activites;
            }
        }
    }

    @Override
    public Collection<SuiviRessource> getSuiviRessources(Long id, Long idProjet) {
        Ressource ressource = ressourceRepository.findOne(id);
        if (ressource == null || ressource.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("Cette ressource n\'existe pas!");
        }
        else {
            return ressource.getSuiviRessources();
        }
    }
}
