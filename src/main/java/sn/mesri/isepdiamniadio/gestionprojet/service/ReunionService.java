package sn.mesri.isepdiamniadio.gestionprojet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Projet;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Reunion;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Utilisateur;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.UtilisateurEvenement;

import java.util.Collection;

public interface ReunionService {
    Page<Reunion> findAll(Pageable pageable);
    Reunion findById(Long id);
    Reunion insert(Reunion reunion);
    Reunion update(Reunion reunion);
    void delete(Long id);
    Collection<Projet> getProjets(Long id);
    Collection<Utilisateur> getUtilisateurs(Long id);
}

