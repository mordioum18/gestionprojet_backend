package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import sn.mesri.isepdiamniadio.gestionprojet.entities.enums.ExtensionDocument;
import sn.mesri.isepdiamniadio.gestionprojet.entities.enums.Statut;
import sn.mesri.isepdiamniadio.gestionprojet.exception.BadRequestException;
import sn.mesri.isepdiamniadio.gestionprojet.exception.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import sn.mesri.isepdiamniadio.gestionprojet.dao.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;

import sn.mesri.isepdiamniadio.gestionprojet.service.DocumentService;
import sn.mesri.isepdiamniadio.gestionprojet.service.SuiviActiviteService;
import sn.mesri.isepdiamniadio.gestionprojet.utils.CopyNotNullPropertiesUtil;

import java.io.File;
import java.util.Collection;
import java.util.Date;

@Service
public class SuiviActiviteServiceImpl implements SuiviActiviteService {
    @Autowired
    private SuiviActiviteRepository suiviActiviteRepository;
    @Autowired
    private ActiviteRepository activiteRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private DocumentService documentService;
    @Autowired
    private ReunionRepository reunionRepository;
    @Autowired
    private VisiteRepository visiteRepository;


    @Override
    public Page<SuiviActivite> findAll(Pageable pageable) {
        return suiviActiviteRepository.findAll(pageable);
    }

    @Override
    public SuiviActivite findById(Long idProjet,Long idSuiviActivite) {
        SuiviActivite suiviActivite=suiviActiviteRepository.findOne(idSuiviActivite);
        if (suiviActivite==null || suiviActivite.getActivite().getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException(" Suivi activité introuvable");
        }
        return  suiviActivite;
    }

    @Override
    public SuiviActivite insert(SuiviActivite suiviActivite,Long idProjet) {
        if (suiviActivite.getActivite() == null){
            throw new ResourceNotFoundException("l\'activité dont vous voulez ajoutez un suivi, n\'existe pas ");
        }
        Activite activite = activiteRepository.findOne(suiviActivite.getActivite().getId());
        if (activite == null || activite.getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("l\'activité dont vous voulez ajoutez un suivi, n\'existe pas ");
        }
        if (suiviActivite.getDateSuivi().before(activite.getDateExecutionPrevisionnelle())){
            throw new BadRequestException("Date incorrecte");
        }
        if (suiviActivite.getNiveauRealisation()>100){
            throw new BadRequestException("Niveau de réalisation incorrect");
        }
        Collection<SuiviActivite> suiviActivites = activite.getSuiviActivites();
        for (SuiviActivite sa: suiviActivites){
            if (sa.getDateSuivi().after(suiviActivite.getDateSuivi()) &&
                    sa.getNiveauRealisation() < suiviActivite.getNiveauRealisation()){
                throw new BadRequestException("Données incorrectes");
            }
        }
        suiviActivite.setActivite(activite);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Utilisateur utilisateur=utilisateurRepository.findByUsername(auth.getName());
        suiviActivite.setUtilisateur(utilisateur);
        suiviActivite.setDateCreation(new Date());
        suiviActivite.setRetard(calculRetard(suiviActivite));
        Collection<Document> documents = suiviActivite.getDocuments();
        suiviActivite.setDocuments(null);
        SuiviActivite s = suiviActiviteRepository.save(suiviActivite);
        if (documents != null){
            for (Document document: documents){
                if (ExtensionDocument.findByName(document.getExtension()) && document.getTypeDocument()!=null || !document.getTypeDocument().equals("")){
                    document.setSuivi(s);
                    if(this.documentService.insertDocument(document, idProjet)==null){
                        this.delete(idProjet, s.getId());
                        throw new BadRequestException("image invalide");
                    }
                }
            }
        }
        if (suiviActivite.getNiveauRealisation() > activite.getTauxExecutionPhysique()){
            activite.setTauxExecutionPhysique(suiviActivite.getNiveauRealisation());
            if (suiviActivite.getNiveauRealisation() == 100){
                activite.setStatutActivite(Statut.DONE.getName());
            }
            if (suiviActivite.getNiveauRealisation() < 100){
                activite.setStatutActivite(Statut.DOING.getName());
            }
            activiteRepository.saveAndFlush(activite);
        }
        s.setDocuments(documents);
        return s;
    }

    @Override
    public void delete(Long idProjet,Long idSuiviActivite) {
        SuiviActivite s=suiviActiviteRepository.findOne(idSuiviActivite);
        Activite activite = s.getActivite();
        if (s==null || activite==null || s.getActivite().getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException(" Suivi activité introuvable");
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Utilisateur utilisateur=utilisateurRepository.findByUsername(auth.getName());
        if (utilisateur.getId() != s.getUtilisateur().getId()){
            throw new ResourceNotFoundException("Suivi activité introuvable");
        }
        if (s.getDocuments()!=null){
            for (Document doc: s.getDocuments()){
                try{

                    File file = new File(doc.getChemin());

                    if(file.delete()){
                        System.out.println(file.getName() + " is deleted!");
                    }else{
                        System.out.println("Delete operation is failed.");
                    }

                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }

        suiviActiviteRepository.delete(idSuiviActivite);
        activite = activiteRepository.findOne(activite.getId());
        Collection<SuiviActivite> suiviActivites = activite.getSuiviActivites();
        int taux = 0;
        int niveau = 0;
        for (SuiviActivite sa: suiviActivites){
            if (taux < sa.getNiveauRealisation()){
                taux = sa.getNiveauRealisation();
            }
            if (niveau < sa.getNiveauRealisation()){
                niveau = sa.getNiveauRealisation();
            }
        }
        activite.setTauxExecutionPhysique(taux);
        if (niveau == 0){
            activite.setStatutActivite(Statut.TODO.getName());
        }
        if (niveau == 100){
            activite.setStatutActivite(Statut.DONE.getName());
        }
        if (niveau < 100){
            activite.setStatutActivite(Statut.DOING.getName());
        }
        activiteRepository.saveAndFlush(activite);

    }

    @Override
    public Collection<Document> getDocuments(Long idProjet, Long idSuiviActivite) {
        SuiviActivite s=suiviActiviteRepository.findOne(idSuiviActivite);
        if (s== null || s.getActivite().getLot().getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("Ce suivi activité n\'a pas été pas!");
        }
        else {
            return s.getDocuments();
        }
    }

    private Long calculRetard(SuiviActivite suiviActivite){
        Activite activite = suiviActivite.getActivite();
        if (suiviActivite.getNiveauRealisation() == 100){
            if (suiviActivite.getDateSuivi().getTime()!=activite.getDateFinPrevisionnelle().getTime()){
                return (suiviActivite.getDateSuivi().getTime() - activite.getDateFinPrevisionnelle().getTime()) / (1000*3600*24);
            }
            else {
                return 0L;
            }
        }
        Long duree = (suiviActivite.getDateSuivi().getTime() - activite.getDateExecutionPrevisionnelle().getTime())/(1000 * 60 * 60 * 24);
        if (suiviActivite.getNiveauRealisation()!=0){
            Long dureeActivite = (100*duree)/suiviActivite.getNiveauRealisation();
            return dureeActivite - activite.getDureeExecutionPrevisionnelle();
        }
        else {
            return duree;
        }
    }
}
