package sn.mesri.isepdiamniadio.gestionprojet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;

import java.util.Collection;

public interface RessourceService {
    Page<Ressource> findAll(Pageable pageable);
    Ressource findById(Long id, Long idProjet);
    Ressource insert(Ressource ressource, Long idProjet);
    Ressource update(Ressource ressource, Long idProjet);
    void delete(Long id, Long idProjet);
    Collection<ActiviteRessource> getActiviteRessources(Long id, Long idProjet);
    Collection<Activite> getActivites(Long id, Long idProjet);
    Collection<SuiviRessource> getSuiviRessources(Long id, Long idProjet);
}
