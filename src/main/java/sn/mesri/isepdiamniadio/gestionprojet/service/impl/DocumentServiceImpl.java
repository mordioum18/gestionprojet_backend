package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.enums.ExtensionDocument;
import sn.mesri.isepdiamniadio.gestionprojet.exception.BadRequestException;
import org.springframework.stereotype.Service;
import sn.mesri.isepdiamniadio.gestionprojet.dao.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;
import sn.mesri.isepdiamniadio.gestionprojet.service.DocumentService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Date;

@Service
public class DocumentServiceImpl implements DocumentService {
    @Autowired
    private DocumentRepository documentRepository;
    @Autowired
    private SuiviActiviteRepository suiviActiviteRepository;
    @Autowired
    private SuiviRessourceRepository suiviLotRepository;
    @Autowired
    private ValidationActiviteRepository validationActiviteRepository;
    @Autowired
    private ValidationLotRepository validationLotRepository;
    @Autowired
    private DecaissementRepository decaissementRepository;

    @Value("${sourceFile}")
    private String sourceFile;

    @Override
    public Page<Document> findAll(Pageable pageable) {
        return documentRepository.findAll(pageable);
    }


    @Override
    public Document findDocumentById(Long idProjet, Long idDocument) {
        Document document=documentRepository.findOne(idDocument);
        if (document.getSuivi()!=null){
            if(document.getSuivi() instanceof SuiviActivite && ((SuiviActivite) document.getSuivi()).getActivite().getLot().getProjet().getId()!=idProjet){
                throw new BadRequestException("document incorrect");
            }
        }
        if (document.getValidation()!=null){
            if(document.getValidation() instanceof ValidationLot && ((ValidationLot) document.getValidation()).getLot().getProjet().getId()!=idProjet){
                throw new BadRequestException("document incorrect");
            }
            if(document.getValidation() instanceof ValidationActivite && ((ValidationActivite) document.getValidation()).getActivite().getLot().getProjet().getId()!=idProjet){
                throw new BadRequestException("document incorrect");
            }
        }
        return document;
    }

    @Override
    public Document insertDocument(Document document, Long idProjet) {
        if (document.getExtension()==null){
            return null;
        }
        if (document.getTypeDocument()==null || document.getTypeDocument().equals("")){
            return null;
        }
        if (document.getSuivi()!=null){
            SuiviRessource suiviLot = suiviLotRepository.findOne(document.getSuivi().getId());
            SuiviActivite suiviActivite = suiviActiviteRepository.findOne(document.getSuivi().getId());
            if (suiviActivite == null && suiviLot == null){
                return null;
            }
            if(suiviActivite!=null && suiviActivite.getActivite().getLot().getProjet().getId()!=idProjet){
                return null;
            }
        }
        if (document.getValidation()!=null){
            ValidationLot validationLot = validationLotRepository.findOne(document.getValidation().getId());
            ValidationActivite validationActivite = validationActiviteRepository.findOne(document.getValidation().getId());
            if (validationActivite == null && validationLot == null){
                return null;
            }
            if(validationLot!=null && validationLot.getLot().getProjet().getId()!=idProjet){
                return null;
            }
            if(validationActivite!=null && validationActivite.getActivite().getLot().getProjet().getId()!=idProjet){
                return null;
            }
        }
        if (document.getDecaissement()!=null){
            Decaissement decaissement = decaissementRepository.findOne(document.getDecaissement().getId());
            if (decaissement == null || decaissement.getActivite().getLot().getProjet().getId()!=idProjet){
                return null;
            }
        }
        if (document.getDecaissement()==null && document.getValidation()==null && document.getSuivi()==null){
            return null;
        }
        document.setDateCreation(new Date());
        Date date= new Date();
        if (ExtensionDocument.findByName(document.getExtension())==false){
            return null;
        }
        File dossier=new File(this.sourceFile+"/projet"+idProjet);
        if (!dossier.exists()){
            dossier.mkdir();
        }
        if (!dossier.isDirectory()){
            dossier.mkdir();
        }
        Long nom = date.getTime();
        if (document.getNom()==null || document.getNom().equals("")){
            document.setNom(nom.toString());
        }
        String source = this.sourceFile+"/projet"+idProjet+"/"+nom+"."+document.getExtension();
        if (decoder(document.getChemin(), source)){
            document.setChemin(source);
            return documentRepository.save(document);
        }
        return null;
    }

    @Override
    public void deleteDocument(Long idProjet, Long idDocument) {
        Document document=documentRepository.findOne(idDocument);
        if (document.getSuivi()!=null){
            if(document.getSuivi() instanceof SuiviActivite && ((SuiviActivite) document.getSuivi()).getActivite().getLot().getProjet().getId()!=idProjet){
                throw new BadRequestException("document incorrect");
            }
        }
        if (document.getValidation()!=null){
            if(document.getValidation() instanceof ValidationLot && ((ValidationLot) document.getValidation()).getLot().getProjet().getId()!=idProjet){
                throw new BadRequestException("document incorrect");
            }
            if(document.getValidation() instanceof ValidationActivite && ((ValidationActivite) document.getValidation()).getActivite().getLot().getProjet().getId()!=idProjet){
                throw new BadRequestException("document incorrect");
            }
        }
        documentRepository.delete(document);
    }

    public static Boolean decoder(String base64Image, String pathFile) {
        try (FileOutputStream imageOutFile = new FileOutputStream(pathFile)) {
            byte[] imageByteArray = Base64.getDecoder().decode(base64Image);
            imageOutFile.write(imageByteArray);
            return true;
        } catch (FileNotFoundException e) {
            System.out.println("Image not found" + e);
            return false;
        } catch (IOException ioe) {
            System.out.println("Exception while reading the Image " + ioe);
            return false;
        }
    }
}
