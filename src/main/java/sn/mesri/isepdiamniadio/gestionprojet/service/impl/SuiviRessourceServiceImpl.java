package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import sn.mesri.isepdiamniadio.gestionprojet.exception.BadRequestException;
import sn.mesri.isepdiamniadio.gestionprojet.exception.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import sn.mesri.isepdiamniadio.gestionprojet.dao.*;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.*;
import sn.mesri.isepdiamniadio.gestionprojet.service.SuiviRessourceService;
import sn.mesri.isepdiamniadio.gestionprojet.utils.CopyNotNullPropertiesUtil;

import java.util.Collection;
import java.util.Date;

@Service
public class SuiviRessourceServiceImpl implements SuiviRessourceService {
    @Autowired
    private SuiviRessourceRepository suiviRessourceRepository;
    @Autowired
    private RessourceRepository ressourceRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Override
    public Page<SuiviRessource> findAll(Pageable pageable) {
        return suiviRessourceRepository.findAll(pageable);
    }

    @Override
    public SuiviRessource findById(Long idProjet, Long idSuiviRessource) {
        SuiviRessource suiviRessource = suiviRessourceRepository.findOne(idSuiviRessource);
        if (suiviRessource == null || suiviRessource.getRessource().getProjet().getId() != idProjet) {
            throw new ResourceNotFoundException("suivi ressource introuvable");
        }
        return suiviRessource;
    }
    @Override
    public SuiviRessource insert(SuiviRessource suiviRessource, Long idProjet) {
        if (suiviRessource.getRessource() == null){
            throw new ResourceNotFoundException("la ressource dont vous voulez ajoutez un suivi, n\'existe pas ");
        }
        Ressource ressource = ressourceRepository.findOne(suiviRessource.getRessource().getId());
        if (ressource == null || ressource.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("la ressource dont vous voulez ajoutez un suivi, n\'existe pas ");
        }
        Collection<SuiviRessource> suiviRessources = ressource.getSuiviRessources();
        for (SuiviRessource sa: suiviRessources){
            if (sa.getDateSuivi().after(suiviRessource.getDateSuivi())) {
                throw new BadRequestException("Données incorrectes");
            }
        }
        suiviRessource.setRessource(ressource);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Utilisateur utilisateur=utilisateurRepository.findByUsername(auth.getName());
        suiviRessource.setUtilisateur(utilisateur);
        suiviRessource.setDateCreation(new Date());
        SuiviRessource s = suiviRessourceRepository.save(suiviRessource);
        return s;
    }

    @Override
    public SuiviRessource update (SuiviRessource suiviRessource, Long idProjet){
        if (suiviRessource.getRessource() == null){
            throw new ResourceNotFoundException("la ressource dont vous voulez ajoutez un suivi, n\'existe pas ");
        }
        Ressource ressource = ressourceRepository.findOne(suiviRessource.getRessource().getId());
        if (ressource == null || ressource.getProjet().getId()!=idProjet){
            throw new ResourceNotFoundException("la ressource dont vous voulez ajoutez un suivi, n\'existe pas ");
        }
        SuiviRessource s = suiviRessourceRepository.findOne(suiviRessource.getId());
        if (s == null || s.getRessource().getProjet().getId()!=idProjet) {
            throw new ResourceNotFoundException("Suivi ressource introuvable");
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Utilisateur utilisateur=utilisateurRepository.findByUsername(auth.getName());
        if (utilisateur.getId() != s.getUtilisateur().getId()){
            throw new ResourceNotFoundException("Suivi ressource introuvable");
        }
        Collection<SuiviRessource> suiviRessources = ressource.getSuiviRessources();
        for (SuiviRessource sa: suiviRessources){
            if (sa.getDateSuivi().after(suiviRessource.getDateSuivi())) {
                throw new BadRequestException("Données incorrectes");
            }
        }
        CopyNotNullPropertiesUtil<SuiviRessource> copy=new CopyNotNullPropertiesUtil<SuiviRessource>();
        copy.copyNonNullProperties(s,suiviRessource);
        s.setDateModification(new Date());
        return suiviRessourceRepository.saveAndFlush(s);
    }

    @Override
    public void delete (Long idProjet, Long idSuiviRessource){
        SuiviRessource s = suiviRessourceRepository.findOne(idSuiviRessource);
        Ressource ressource = s.getRessource();
        if (s == null || s.getRessource().getProjet().getId()!=idProjet) {
            throw new ResourceNotFoundException("Suivi ressource introuvable");
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Utilisateur utilisateur=utilisateurRepository.findByUsername(auth.getName());
        if (utilisateur.getId() != s.getUtilisateur().getId()){
            throw new ResourceNotFoundException("Suivi activité introuvable");
        }
        suiviRessourceRepository.delete(idSuiviRessource);
    }


}
