package sn.mesri.isepdiamniadio.gestionprojet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Projet;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Utilisateur;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.UtilisateurEvenement;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Visite;

import java.util.Collection;

public interface VisiteService {
    Page<Visite> findAll(Pageable pageable);
    Visite findById(Long id);
    Visite insert(Visite visite, Long idUtilisateur);
    Visite update(Visite visite);
    void delete(Long id);
    Collection<Projet> getProjets(Long id);
    Collection<Utilisateur> getUtilisateurs(Long id);
}

