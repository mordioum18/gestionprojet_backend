package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.exception.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import sn.mesri.isepdiamniadio.gestionprojet.dao.TypeProjetRepository;
import sn.mesri.isepdiamniadio.gestionprojet.dao.TypeProjetRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Projet;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.TypeProjet;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.TypeProjet;
import sn.mesri.isepdiamniadio.gestionprojet.service.TypeProjetService;
import sn.mesri.isepdiamniadio.gestionprojet.service.TypeProjetService;

import java.util.Collection;
import java.util.Date;

@Service
public class TypeProjetServiceImpl implements TypeProjetService {
    @Autowired
    private TypeProjetRepository typeProjetRepository;


        @Override
        public Collection<TypeProjet> findAll() {
            return typeProjetRepository.findAll();
        }

        @Override
        public TypeProjet findById(Long id) {
            TypeProjet typeProjet=typeProjetRepository.findOne(id);
            if (typeProjet==null){
                throw new ResourceNotFoundException("Type projet introuvable");
            }
            return typeProjet;
        }

        @Override
        public TypeProjet insert(TypeProjet typeProjet) {
            typeProjet.setDateCreation(new Date());
            return typeProjetRepository.save(typeProjet);
        }

        @Override
        public TypeProjet update(TypeProjet typeProjet) {
            TypeProjet tp=typeProjetRepository.findOne(typeProjet.getId());
            if (tp==null){
                throw new ResourceNotFoundException("Type projet introuvable");
            }
            typeProjet.setDateModification(new Date());
            return typeProjetRepository.saveAndFlush(typeProjet);
        }

        @Override
        public void delete(Long id) {
            TypeProjet tp=typeProjetRepository.findOne(id);
            if (tp==null){
                throw new ResourceNotFoundException("Type projet introuvable");
            }
            typeProjetRepository.delete(id);

        }

    @Override
    public Collection<Projet> getProjets(Long id) {
        if (typeProjetRepository.findOne(id) == null){
            throw new ResourceNotFoundException("Ce type de projet n\'existe pas!");
        } else {
            return typeProjetRepository.findOne(id).getProjets();
        }
    }
}

