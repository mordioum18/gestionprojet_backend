package sn.mesri.isepdiamniadio.gestionprojet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import sn.mesri.isepdiamniadio.gestionprojet.dao.EvenementRepository;
import sn.mesri.isepdiamniadio.gestionprojet.dao.UtilisateurRepository;
import sn.mesri.isepdiamniadio.gestionprojet.dao.VisiteRepository;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Projet;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Utilisateur;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.UtilisateurEvenement;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Visite;
import sn.mesri.isepdiamniadio.gestionprojet.exception.ResourceNotFoundException;
import sn.mesri.isepdiamniadio.gestionprojet.service.VisiteService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Service
public class VisiteServiceImpl implements VisiteService{
    @Autowired
    private VisiteRepository visiteRepository;
    @Autowired
    private EvenementRepository EvenementRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Override
    public Page<Visite> findAll(Pageable pageable) {
        return visiteRepository.findAll(pageable);
    }

    @Override
    public Visite findById(Long id) {
        Visite visite=visiteRepository.findOne(id);
        if (visite==null){
            throw new ResourceNotFoundException("Enregistrement introuvable");
        }
        return visite;
    }

    @Override
    public Visite insert(Visite visite, Long idUtilisateur) {
        Utilisateur utilisateur = utilisateurRepository.findOne(idUtilisateur);
        if (utilisateur == null){
            throw new ResourceNotFoundException("Cet utilisateur qui veut créer une visite, n\'existe pas ");
        } else {
            visite.setDateCreation(new Date());
            visite.setUtilisateur(utilisateur);
            return visiteRepository.save(visite);
        }
    }

    @Override
    public Visite update(Visite visite) {
        Visite v=visiteRepository.findOne(visite.getId());
        if (v==null){
            throw new ResourceNotFoundException("Enregistrement introuvable");
        }
        visite.setDateModification(new Date());
        return visiteRepository.saveAndFlush(visite);
    }

    @Override
    public void delete(Long id) {
        Visite r=visiteRepository.findOne(id);
        if (r==null){
            throw new ResourceNotFoundException("Enregistrement introuvable");
        }
        visiteRepository.delete(id);

    }

    @Override
    public Collection<Projet> getProjets(Long id) {
        if (visiteRepository.findOne(id) == null){
            throw new ResourceNotFoundException("Cette visite n\'a pas encore été initiée!");
        }
        else {
            return visiteRepository.findOne(id).getProjets();
        }
    }

    @Override
    public Collection<Utilisateur> getUtilisateurs(Long id) {
        Visite visite=visiteRepository.findOne(id);
        if ( visite== null){
            throw new ResourceNotFoundException("Cette visite n\'a pas encore été initiée!");
        }
        Collection<Utilisateur> utilisateurs=new ArrayList<Utilisateur>();
        for(UtilisateurEvenement ue:visite.getUtilisateurEvenements()){
            utilisateurs.add(ue.getPk().getUtilisateur());
        }
        return utilisateurs;
    }
}
