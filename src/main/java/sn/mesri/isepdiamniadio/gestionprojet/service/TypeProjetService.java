package sn.mesri.isepdiamniadio.gestionprojet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.Projet;
import sn.mesri.isepdiamniadio.gestionprojet.entities.model.TypeProjet;
import java.util.Collection;

public interface TypeProjetService {
    Collection<TypeProjet> findAll();
    TypeProjet findById(Long id);
    TypeProjet insert(TypeProjet typeProjet);
    TypeProjet update(TypeProjet typeProjet);
    void delete(Long id);
    Collection<Projet> getProjets(Long id);
}

