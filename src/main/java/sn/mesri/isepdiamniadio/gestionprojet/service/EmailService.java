package sn.mesri.isepdiamniadio.gestionprojet.service;

public interface EmailService {
    void sendSimpleMessage(String to, String subject, String text);
}
